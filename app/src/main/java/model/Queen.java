package model;

import android.widget.ImageView;

import java.util.ArrayList;

public class Queen extends Piece {

   public String name;
   public boolean white;
   public ImageView figImage;

   public String getName(){
      return name;
   }

   public Square[][] move(String coordinates, Square[][] testBoard, int turnCount, Square[][] clearLayout){

      String initialPosition = coordinates.substring(0, 2);
      String newPosition = coordinates.substring(3,5);
      int oldCol = (int)(initialPosition.charAt(0)-97);
      int newCol = (int)(newPosition.charAt(0)-97);
      int oldRow = 8-(int)(initialPosition.charAt(1)-48);
      int newRow = 8-(int)(newPosition.charAt(1)-48);

      Square[][] board = testBoard;

      //Making sure that the new coordinates are not out of bounds:
      if(newCol > 7 || newCol < 0 || newRow > 7 || newRow < 0){
         //System.out.println("Out of bounds");
         return board; //Out of bounds
      }
      //Make sure not attacking your own team
      if(!(board[newRow][newCol].getPiece() instanceof NonPiece)) {
         if (board[newRow][newCol].getPiece().getTeam() == board[oldRow][oldCol].getPiece().getTeam()) {
            return board;
         }
      }

      //Handling diagonal moves:
      if((oldCol != newCol && oldRow != newRow) && (Math.abs(newRow - oldRow) == Math.abs(newCol - oldCol))){

         if(oldCol > newCol && oldRow > newRow){ //Going Up and Left

            int i = oldRow - 1;
            int j = oldCol - 1;

            while(i !=newRow && j != newCol){

               if(!(board[i][j].getPiece() instanceof NonPiece)){
                  System.out.println("Up and Left Jump on coordinates: " + i + j);
                  return board; //Illegal move, jumping over pieces.
               }

               i--;
               j--;

            }

				/*Placing (or attacking) in the new spot, clearing the old spot:

				if(board[newRow][newCol].getPiece().getTeam() == board[oldRow][oldCol].getPiece().getTeam()){
					return false; //Attacking your own team
				}*/

            board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
            board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
            return board;

         }else if(oldCol < newCol && oldRow > newRow){ //Going Up and Right

            int i = oldRow -1;
            int j = oldCol + 1;

            while(i != newRow && j != newCol){
               if(!(board[i][j].getPiece() instanceof NonPiece)){
                  System.out.println("Up and Right jump on coordinates: " + i + j);
                  return board; //Illegal move, jumping over pieces.
               }

               i--;
               j++;

            }

            //Placing (or attacking) in the new spot, clearing the old spot:

            if(board[newRow][newCol].getPiece().getTeam() == board[oldRow][oldCol].getPiece().getTeam()){
               return board; //Attacking your own team
            }

            board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
            board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
            return board;

         }else if(oldCol < newCol && oldRow < newRow){ //Going Down and Right

            int i = oldRow + 1;
            int j = oldCol + 1;

            while(i != newRow && j != newCol){
               if(!(board[i][j].getPiece() instanceof NonPiece)){
                  System.out.println("Down and Right jump on coordinates: " + i + j);
                  return board; //Illegal move, jumping over pieces.
               }

               i++;
               j++;
            }

				/*Placing (or attacking) in the new spot, clearing the old spot:

				if(board[newRow][newCol].getPiece().getTeam() == board[oldRow][oldCol].getPiece().getTeam()){
					return false; //Attacking your own team
				}*/

            board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
            board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
            return board;

         }else if(oldCol > newCol && oldRow < newRow){ //Going Down and Left

            int i = oldRow + 1;
            int j = oldCol - 1;

            while(i != newRow && j != newCol){
               if(!(board[i][j].getPiece() instanceof NonPiece)){
                  System.out.println("Down and Left jump on coordinates: " + i + j);
                  return board; //Illegal move, jumping over pieces.
               }

               i++;
               j--;
            }

            //Placing (or attacking) in the new spot, clearing the old spot:

				/*if(board[newRow][newCol].getPiece().getTeam() == board[oldRow][oldCol].getPiece().getTeam()){

					System.out.println("Attack Down and Left");
					return false; //Attacking your own team
				}*/

            board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
            board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
            return board;

         }



      }else{ //Handling non-diagonal moves.

         //Horizontal move:

         if(oldRow == newRow){

            int temp = oldCol;

            if(oldCol > newCol){ //Going left

               temp = oldCol - 1;

               while(temp != newCol){

                  if(!(board[newRow][temp].getPiece() instanceof NonPiece)){

                     return board; //Jumping over other pieces

                  }

                  temp--;

               }

            }else if(oldCol < newCol){ //Going right

               temp = oldCol + 1;

               while(temp != newCol){

                  if(!(board[newRow][temp].getPiece() instanceof NonPiece)){

                     return board; //Jumping over other pieces

                  }

                  temp++;
               }

            }


            //Placing (or attacking) in the new spot, clearing the old spot:

				/*if(board[newRow][newCol].getPiece().getTeam() == board[oldRow][oldCol].getPiece().getTeam()){
					return false; //Attacking your own team
				}*/

            board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
            board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
            return board;
         }

         //Vertical move:

         if(oldCol == newCol){

            int temp1 = oldRow;

            if(oldRow > newRow){

               temp1 = oldRow - 1;

               while(temp1 != newRow){ //Going up the board

                  if(!(board[temp1][oldCol].getPiece() instanceof NonPiece)){

                     return board; //Jumping over other pieces

                  }

                  temp1--;


               }

            }else if(oldRow < newRow){

               temp1 = oldRow + 1;

               while(temp1 != newRow){ //Going down the board

                  if(!(board[temp1][oldCol].getPiece() instanceof NonPiece)){

                     return board; //Jumping over other pieces

                  }

                  temp1++;

               }

            }

            //Placing (or attacking) in the new spot, clearing the old spot:

				/*if(board[newRow][newCol].getPiece().getTeam() == board[oldRow][oldCol].getPiece().getTeam()){

					return false; //Attacking your own team

				}*/

            board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
            board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
            return board;
         }


      }

      return board;
   }

   public boolean getTeam(){
      return white;
   }

   @Override
   public ArrayList<String> populateTrajectory(int row, int col, Square[][] mainBoard, int[][] controlBoard) {
      int oppTeam;
       ArrayList<String> moves = new ArrayList<>();
       String coord;
      //dont populate trajectory if takeable by defenders
      if(white){
         oppTeam=2;
         if(controlBoard[row][col]==2)
            return moves;
      }else{
         oppTeam=1;
         if(controlBoard[row][col]==1)
            return moves;
      }
      boolean collision = false;
      int rowTemp = row+1;
      int colTemp = col+1;

      //Populating the diagonal trajectory (Down and Right):
      while(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0
              && mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece
              && controlBoard[rowTemp][colTemp]!=oppTeam){
          controlBoard[rowTemp][colTemp] = controlBoard[row][col];
          if(mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece) {
              coord = Integer.toString(rowTemp).concat(Integer.toString(colTemp));
              moves.add(coord);
          }
         rowTemp++;
         colTemp++;
      }
      if(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0){
         controlBoard[rowTemp][colTemp] = controlBoard[row][col];
          if(mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece) {
              coord = Integer.toString(rowTemp).concat(Integer.toString(colTemp));
              moves.add(coord);
          }
         //Checking for a collision with a King:
         if((mainBoard[rowTemp][colTemp].getPiece() instanceof King)
                 && mainBoard[rowTemp][colTemp].getPiece().getTeam()!=white){
            collision = true;
            if(rowTemp+1<8 && colTemp+1<8)
               controlBoard[rowTemp+1][colTemp+1]=controlBoard[row][col];
         }
      }
      //Resetting rowTemp and colTemp:
      rowTemp = row-1;
      colTemp = col-1;

      //Populating the diagonal trajectory (Up and Left):

      while(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0
              && mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece
              && controlBoard[rowTemp][colTemp]!=oppTeam){
         controlBoard[rowTemp][colTemp] = controlBoard[row][col];
          if(mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece) {
              coord = Integer.toString(rowTemp).concat(Integer.toString(colTemp));
              moves.add(coord);
          }
         rowTemp--;
         colTemp--;
      }
      if(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0){
         controlBoard[rowTemp][colTemp] = controlBoard[row][col];
          if(mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece) {
              coord = Integer.toString(rowTemp).concat(Integer.toString(colTemp));
              moves.add(coord);
          }
         //Checking for a collision with a King:
         if((mainBoard[rowTemp][colTemp].getPiece() instanceof King)
                 && mainBoard[rowTemp][colTemp].getPiece().getTeam()!=white){
            collision = true;
            if(rowTemp-1>=0 && colTemp-1>=0)
               controlBoard[rowTemp-1][colTemp-1]=controlBoard[row][col];
         }
      }
      //Resetting rowTemp and colTemp:
      rowTemp = row-1;
      colTemp = col+1;

      //Populating the diagonal trajectory (Up and Right):

      while(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0
              && mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece
              && controlBoard[rowTemp][colTemp]!=oppTeam){
         controlBoard[rowTemp][colTemp] = controlBoard[row][col];
          if(mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece) {
              coord = Integer.toString(rowTemp).concat(Integer.toString(colTemp));
              moves.add(coord);
          }
         rowTemp--;
         colTemp++;
      }
      if(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0){
         controlBoard[rowTemp][colTemp] = controlBoard[row][col];
          if(mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece) {
              coord = Integer.toString(rowTemp).concat(Integer.toString(colTemp));
              moves.add(coord);
          }
         //Checking for a collision with a King:
         if((mainBoard[rowTemp][colTemp].getPiece() instanceof King)
                 && mainBoard[rowTemp][colTemp].getPiece().getTeam()!=white){
            collision = true;
            if(rowTemp-1>=0 && colTemp+1<8)
               controlBoard[rowTemp-1][colTemp+1]=controlBoard[row][col];
         }
      }

      //Resetting rowTemp and colTemp:
      rowTemp = row+1;
      colTemp = col-1;

      //Populating the diagonal trajectory (Down and Left):

      while(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0
              && mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece
              && controlBoard[rowTemp][colTemp]!=oppTeam){
         controlBoard[rowTemp][colTemp] = controlBoard[row][col];
          if(mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece) {
              coord = Integer.toString(rowTemp).concat(Integer.toString(colTemp));
              moves.add(coord);
          }
         rowTemp++;
         colTemp--;
      }
      if(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0){
         controlBoard[rowTemp][colTemp] = controlBoard[row][col];
          if(mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece) {
              coord = Integer.toString(rowTemp).concat(Integer.toString(colTemp));
              moves.add(coord);
          }
         //Checking for a collision with a King:
         if((mainBoard[rowTemp][colTemp].getPiece() instanceof King)
                 && mainBoard[rowTemp][colTemp].getPiece().getTeam()!=white){
            collision = true;
            if(rowTemp+1<8 && colTemp-1>=0)
               controlBoard[rowTemp+1][colTemp-1]=controlBoard[row][col];

         }
      }

      rowTemp = row+1;
      colTemp = col+1;

      //Populating the horizontal trajectory right:
      while(colTemp < 8 && (mainBoard[row][colTemp].getPiece() instanceof NonPiece)
              &&  controlBoard[row][colTemp]!=oppTeam){
         controlBoard[row][colTemp] = controlBoard[row][col];
          coord = Integer.toString(row).concat(Integer.toString(colTemp));
          moves.add(coord);
         colTemp++;
      }
      if(colTemp < 8){
         controlBoard[row][colTemp] = controlBoard[row][col];
         //Checking to see if there was a collision with a King:
         if((mainBoard[row][colTemp].getPiece() instanceof King)
                 && mainBoard[row][colTemp].getPiece().getTeam()!=white){
            collision = true;
            if(colTemp+1<8)
               controlBoard[row][colTemp+1]=controlBoard[row][col];

         }
      }

      //Populating the vertical trajectory down:
      while(rowTemp < 8 &&(mainBoard[rowTemp][col].getPiece() instanceof NonPiece)
              &&  controlBoard[rowTemp][col]!=oppTeam){
         controlBoard[rowTemp][col] = controlBoard[row][col];
          coord = Integer.toString(rowTemp).concat(Integer.toString(col));
          moves.add(coord);
         rowTemp++;
      }
      if(rowTemp < 8){
         controlBoard[rowTemp][col] = controlBoard[row][col];
         //Checking to see if there was a collision with a King:
         if((mainBoard[rowTemp][col].getPiece() instanceof King)
                 && mainBoard[rowTemp][col].getPiece().getTeam()!=white){
            collision = true;
            if(rowTemp+1<8)
               controlBoard[rowTemp+1][col]=controlBoard[row][col];

         }
      }
      //reset row and col temp position
      rowTemp = row-1;
      colTemp = col-1;

      //Populating the horizontal trajectory left:
      while(colTemp >=0 && (mainBoard[row][colTemp].getPiece() instanceof NonPiece)
              && controlBoard[row][colTemp]!=oppTeam){
         controlBoard[row][colTemp] = controlBoard[row][col];
          coord = Integer.toString(row).concat(Integer.toString(colTemp));
          moves.add(coord);
         colTemp--;
      }
      if(colTemp >=0){
         controlBoard[row][colTemp] = controlBoard[row][col];
         //Checking to see if there was a collision with a King:
         if((mainBoard[row][colTemp].getPiece() instanceof King)
                 && mainBoard[row][colTemp].getPiece().getTeam()!=white){
            collision = true;
            if(colTemp-1>=0)
               controlBoard[row][colTemp-1]=controlBoard[row][col];

         }
      }

      //Populating the vertical trajectory up:
      while(rowTemp >=0 && (mainBoard[rowTemp][col].getPiece() instanceof NonPiece)
              && controlBoard[rowTemp][col]!=oppTeam){
         controlBoard[rowTemp][col] = controlBoard[row][col];
          coord = Integer.toString(rowTemp).concat(Integer.toString(col));
          moves.add(coord);
         rowTemp--;
      }
      if(rowTemp >=0){
         controlBoard[rowTemp][col] = controlBoard[row][col];
         //Checking to see if there was a collision with a King:
         if((mainBoard[rowTemp][col].getPiece() instanceof King)
                 && mainBoard[rowTemp][col].getPiece().getTeam()!=white){
            collision = true;
            if(rowTemp-1>=0)
               controlBoard[rowTemp-1][col]=controlBoard[row][col];

         }
      }

      return moves;
   }
}
