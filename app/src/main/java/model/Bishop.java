package model;

import android.widget.ImageView;

import java.util.ArrayList;

public class Bishop extends Piece {

    public String name;
    public boolean white;
    public ImageView figImage;

    public String getName(){
        return name;
    }

    public Square[][] move(String coordinates, Square[][] testBoard, int turnCount, Square[][] clearLayout){
        String initialPosition = coordinates.substring(0, 2);
        String newPosition = coordinates.substring(3,5);
        int oldCol = (int)(initialPosition.charAt(0)-97);
        int newCol = (int)(newPosition.charAt(0)-97);
        int oldRow = 8-(int)(initialPosition.charAt(1)-48);
        int newRow = 8-(int)(newPosition.charAt(1)-48);

        Square[][] board = testBoard;

        //Making sure that the new coordinates are not out of bounds:
        if(newCol > 7 || newCol < 0 || newRow > 7 || newRow < 0){
            System.out.println("Out of bounds");
            return board; //Out of bounds
        }
        //Make sure not attacking your own team
        if(!(board[newRow][newCol].getPiece() instanceof NonPiece)) {
            if (board[newRow][newCol].getPiece().getTeam() == board[oldRow][oldCol].getPiece().getTeam()) {
                return board;
            }
        }

        //Making sure that the move is diagonal and the column and row are changed by a common factor:
        if((oldCol != newCol && oldRow != newRow) && (Math.abs(newRow - oldRow) == Math.abs(newCol - oldCol))){

            if(oldCol > newCol && oldRow > newRow){ //Going Up and Left

                int i = oldRow - 1;
                int j = oldCol - 1;

                while(i !=newRow && j != newCol){

                    if(!(board[i][j].getPiece() instanceof NonPiece)){
                        System.out.println("Up and Left Jump on coordinates: " + i + j);
                        return board; //Illegal move, jumping over pieces.
                    }

                    i--;
                    j--;

                }

                //Placing (or attacking) in the new spot, clearing the old spot:

				/*if(board[newRow][newCol].getPiece().getTeam() == board[oldRow][oldCol].getPiece().getTeam()){
					return false; //Attacking your own team
				}*/

                board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
                board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
                return board;

            }else if(oldCol < newCol && oldRow > newRow){ //Going Up and Right

                int i = oldRow -1;
                int j = oldCol + 1;

                while(i != newRow && j != newCol){
                    if(!(board[i][j].getPiece() instanceof NonPiece)){
                        System.out.println("Up and Right jump on coordinates: " + i + j);
                        return board; //Illegal move, jumping over pieces.
                    }

                    i--;
                    j++;

                }



                //Placing (or attacking) in the new spot, clearing the old spot:

				/*if(board[newRow][newCol].getPiece().getTeam() == board[oldRow][oldCol].getPiece().getTeam()){
					return false; //Attacking your own team
				}*/

                board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
                board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
                return board;

            }else if(oldCol < newCol && oldRow < newRow){ //Going Down and Right

                int i = oldRow + 1;
                int j = oldCol + 1;

                while(i != newRow && j != newCol){
                    if(!(board[i][j].getPiece() instanceof NonPiece)){
                        System.out.println("Down and Right jump on coordinates: " + i + j);
                        return board; //Illegal move, jumping over pieces.
                    }

                    i++;
                    j++;
                }



				/*Placing (or attacking) in the new spot, clearing the old spot:

				if(board[newRow][newCol].getPiece().getTeam() == board[oldRow][oldCol].getPiece().getTeam()){
					return false; //Attacking your own team
				}*/

                board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
                board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
                return board;

            }else if(oldCol > newCol && oldRow < newRow){ //Going Down and Left

                int i = oldRow + 1;
                int j = oldCol - 1;

                while(i != newRow && j != newCol){
                    if(!(board[i][j].getPiece() instanceof NonPiece)){
                        System.out.println("Down and Left jump on coordinates: " + i + j);
                        return board; //Illegal move, jumping over pieces.
                    }

                    i++;
                    j--;
                }




                //Placing (or attacking) in the new spot, clearing the old spot:

				/*if(board[newRow][newCol].getPiece().getTeam() == board[oldRow][oldCol].getPiece().getTeam()){

					System.out.println("Attack Down and Left");
					return false; //Attacking your own team
				}*/

                board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
                board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
                return board;

            }



        }else{

            System.out.println("Not a diagonal");
            return board; //The trajectory is not on a diagonal.

        }

        return board;
    }

    public boolean getTeam(){
        return white;
    }

    @Override
    public ArrayList<String> populateTrajectory(int row, int col, Square[][] mainBoard, int[][] controlBoard) {
        int oppTeam;
        String coord;
        ArrayList<String> moves = new ArrayList<>();
        //dont populate trajectory if takeable by defenders
        if(white){
            oppTeam=2;
            if(controlBoard[row][col]==2)
                return moves;
        }else{
            oppTeam=1;
            if(controlBoard[row][col]==1)
                return moves;
        }
        boolean collision = false;
        int rowTemp = row+1;
        int colTemp = col+1;

        //Populating the diagonal trajectory (Down and Right):
        while(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0
                && mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece
                && controlBoard[rowTemp][colTemp]!=oppTeam){
            controlBoard[rowTemp][colTemp] = controlBoard[row][col];
            if(mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece) {
                coord = Integer.toString(rowTemp).concat(Integer.toString(colTemp));
                moves.add(coord);
            }
            rowTemp++;
            colTemp++;
        }
        if(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0){
            controlBoard[rowTemp][colTemp] = controlBoard[row][col];
            if(mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece) {
                coord = Integer.toString(rowTemp).concat(Integer.toString(colTemp));
                moves.add(coord);
            }
            //Checking for a collision with a King:
            if((mainBoard[rowTemp][colTemp].getPiece() instanceof King)
                    && mainBoard[rowTemp][colTemp].getPiece().getTeam()!=white){
                collision = true;
                if(rowTemp+1<8 && colTemp+1<8)
                    controlBoard[rowTemp+1][colTemp+1]=controlBoard[row][col];
            }
        }
        //Resetting rowTemp and colTemp:
        rowTemp = row-1;
        colTemp = col-1;

        //Populating the diagonal trajectory (Up and Left):

        while(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0
                && mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece
                && controlBoard[rowTemp][colTemp]!=oppTeam){
            controlBoard[rowTemp][colTemp] = controlBoard[row][col];
            if(mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece) {
                coord = Integer.toString(rowTemp).concat(Integer.toString(colTemp));
                moves.add(coord);
            }
            rowTemp--;
            colTemp--;
        }
        if(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0){
            controlBoard[rowTemp][colTemp] = controlBoard[row][col];
            if(mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece) {
                coord = Integer.toString(rowTemp).concat(Integer.toString(colTemp));
                moves.add(coord);
            }
            //Checking for a collision with a King:
            if((mainBoard[rowTemp][colTemp].getPiece() instanceof King)
                    && mainBoard[rowTemp][colTemp].getPiece().getTeam()!=white){
                collision = true;
                if(rowTemp-1>=0 && colTemp-1>=0)
                    controlBoard[rowTemp-1][colTemp-1]=controlBoard[row][col];
            }
        }
        //Resetting rowTemp and colTemp:
        rowTemp = row-1;
        colTemp = col+1;

        //Populating the diagonal trajectory (Up and Right):

        while(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0
                && mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece
                && controlBoard[rowTemp][colTemp]!=oppTeam){
            controlBoard[rowTemp][colTemp] = controlBoard[row][col];
            if(mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece) {
                coord = Integer.toString(rowTemp).concat(Integer.toString(colTemp));
                moves.add(coord);
            }
            rowTemp--;
            colTemp++;
        }
        if(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0){
            controlBoard[rowTemp][colTemp] = controlBoard[row][col];
            if(mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece) {
                coord = Integer.toString(rowTemp).concat(Integer.toString(colTemp));
                moves.add(coord);
            }
            //Checking for a collision with a King:
            if((mainBoard[rowTemp][colTemp].getPiece() instanceof King)
                    && mainBoard[rowTemp][colTemp].getPiece().getTeam()!=white){
                collision = true;
                if(rowTemp-1>=0 && colTemp+1<8)
                    controlBoard[rowTemp-1][colTemp+1]=controlBoard[row][col];
            }
        }

        //Resetting rowTemp and colTemp:
        rowTemp = row+1;
        colTemp = col-1;

        //Populating the diagonal trajectory (Down and Left):

        while(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0
                && mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece
                && controlBoard[rowTemp][colTemp]!=oppTeam){
            controlBoard[rowTemp][colTemp] = controlBoard[row][col];
            if(mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece) {
                coord = Integer.toString(rowTemp).concat(Integer.toString(colTemp));
                moves.add(coord);
            }
            rowTemp++;
            colTemp--;
        }
        if(rowTemp < 8 && colTemp < 8 && rowTemp >= 0 && colTemp >= 0){
            controlBoard[rowTemp][colTemp] = controlBoard[row][col];
            if(mainBoard[rowTemp][colTemp].getPiece() instanceof NonPiece) {
                coord = Integer.toString(rowTemp).concat(Integer.toString(colTemp));
                moves.add(coord);
            }
            //Checking for a collision with a King:
            if((mainBoard[rowTemp][colTemp].getPiece() instanceof King)
                    && mainBoard[rowTemp][colTemp].getPiece().getTeam()!=white){
                collision = true;
                if(rowTemp+1<8 && colTemp-1>=0)
                    controlBoard[rowTemp+1][colTemp-1]=controlBoard[row][col];
            }
        }

        return moves;
    }
}
