package model;

import android.widget.ImageView;

import java.util.ArrayList;

public class Pawn extends Piece {

    public String name;
    public boolean white;
    public ImageView figImage;
    public int enpTurn;

    public String getName(){
        return name;
    }

    public Square[][] move(String coordinates, Square[][] testBoard, int turnCount, Square[][] clearLayout){

        //parse input string
        String initialPosition = coordinates.substring(0, 2);
        String newPosition = coordinates.substring(3);
        int oldCol = (int)(initialPosition.charAt(0)-97);
        int newCol = (int)(newPosition.charAt(0)-97);
        int oldRow = 8-(int)(initialPosition.charAt(1)-48);
        int newRow = 8-(int)(newPosition.charAt(1)-48);
        boolean legal = false;
        enpTurn=-1;

        Square[][] board = testBoard;

        //check for coordinates out of bounds:

        if(newCol > 7 || newCol < 0 || newRow > 7 || newRow < 0)
            return board; //return false

        if(white){							//white starts at bottom
            if(newRow==oldRow-1){
                if(newCol==oldCol){			//legal move 1 space forward, if space is free
                    if(board[newRow][newCol].getPiece() instanceof NonPiece){
                        legal=true;
                    }
                }else if(newCol==oldCol+1 || newCol==oldCol-1){
                    //attacking, check for black piece
                    if(!(board[newRow][newCol].getPiece() instanceof NonPiece) && board[newRow][newCol].getPiece().getTeam() != white){
                        legal=true;
                    }else if(board[oldRow][newCol].getPiece() instanceof Pawn){
                        //check pawn for enpassant, take if legal
                        Pawn tmp = (Pawn) board[oldRow][newCol].getPiece();
                        if(tmp.enpTurn==turnCount-1 && !tmp.getTeam()){
                            board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
                            board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
                            board[oldRow][newCol].setPiece(clearLayout[oldRow][newCol].getPiece());
                            return board; //return true
                        }
                    }
                }
            }else if(newRow==4&&oldRow==6){	//first move can be two spaces if spaces are free, set en passant status
                if(newCol==oldCol){
                    if(board[newRow][newCol].getPiece() instanceof NonPiece && board[newRow+1][newCol].getPiece() instanceof NonPiece){
                        legal=true;
                        enpTurn=turnCount;
                    }
                }
            }//end white pawn
        }else{								//black starts at top
            if(newRow==oldRow+1){
                if(newCol==oldCol){			//legal move 1 space down, if space is free
                    if(board[newRow][newCol].getPiece() instanceof NonPiece){
                        legal=true;
                    }
                }else if(newCol==oldCol+1 || newCol==oldCol-1){
                    //attacking, check for white piece
                    if(!(board[newRow][newCol].getPiece() instanceof NonPiece) && board[newRow][newCol].getPiece().getTeam()!=white){
                        legal=true;
                    }else if(board[oldRow][newCol].getPiece() instanceof Pawn){
                        //check pawn for enpassant, take if legal
                        Pawn tmp = (Pawn) board[oldRow][newCol].getPiece();
                        if(tmp.enpTurn==turnCount-1 && tmp.getTeam()){
                            board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
                            board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
                            board[oldRow][newCol].setPiece(clearLayout[oldRow][newCol].getPiece());
                            return board; //return true
                        }
                    }
                }
            }else if(newRow==3&&oldRow==1){	//first move can be two spaces
                if(newCol==oldCol){
                    if(board[newRow][newCol].getPiece() instanceof NonPiece && board[newRow-1][newCol].getPiece() instanceof NonPiece){
                        legal=true;
                        enpTurn=turnCount;
                    }
                }
            }
        }//end black pawn

        if(legal){							//if the move was legal, move the piece, otherwise throw error
            board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
            board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
        }
        if(newRow==0 || newRow==7){			//check for promotion
            char promote='0';
            String newName;
            if(white)
                newName="w";
            else
                newName="b";
            if (coordinates.length()>=7)
                promote = coordinates.charAt(6);
            switch(promote){
                case 'N': 	Knight pnight = new Knight();
                    pnight.white = white;
                    newName+=promote;
                    pnight.name = newName;
                    board[newRow][newCol].setPiece(pnight);
                    break;
                case 'B': 	Bishop pbishop = new Bishop();
                    pbishop.white = white;
                    newName+=promote;
                    pbishop.name = newName;
                    board[newRow][newCol].setPiece(pbishop);
                    break;
                case 'R': 	Rook prook = new Rook();
                    prook.white = white;
                    newName+=promote;
                    prook.name = newName;
                    board[newRow][newCol].setPiece(prook);
                    break;
                default: 	Queen pqueen = new Queen();
                    //Default to queen promotion for none or invalid promotion input
                    pqueen.white = white;
                    newName+='Q';
                    pqueen.name = newName;
                    board[newRow][newCol].setPiece(pqueen);
                    break;
            }
        }//end promotion
        return board;
    }

    public boolean getTeam(){
        return white;
    }

    @Override
    public ArrayList<String> populateTrajectory(int row, int col, Square[][] mainBoard, int[][] controlBoard) {
        //dont populate trajectory if takeable by defenders
        ArrayList<String> moves = new ArrayList<String>();
        if(white){
            if(controlBoard[row][col]==2)
                return moves;
        }else{
            if(controlBoard[row][col]==1)
                return moves;
        }
        boolean collision=false;
        String coord;
        if(white){
            if(row-1>=0){
                if((mainBoard[row-1][col].getPiece() instanceof NonPiece))
                    controlBoard[row-1][col] = controlBoard[row][col];
                    coord = Integer.toString(row-1).concat(Integer.toString(col));
                    moves.add(coord);
                if(col+1<8){
                    if(!(mainBoard[row-1][col+1].getPiece() instanceof NonPiece) && mainBoard[row-1][col+1].getPiece().getTeam()!=white){
                        controlBoard[row-1][col+1] = controlBoard[row][col];
                        coord = Integer.toString(row-1).concat(Integer.toString(col+1));
                        moves.add(coord);
                        if(mainBoard[row-1][col+1].getPiece() instanceof King)
                            collision=true;
                    }
                }
                if(col-1>=0){
                    if(!(mainBoard[row-1][col-1].getPiece() instanceof NonPiece) && mainBoard[row-1][col-1].getPiece().getTeam()!=white){
                        controlBoard[row-1][col-1] = controlBoard[row][col];
                        coord = Integer.toString(row-1).concat(Integer.toString(col-1));
                        moves.add(coord);
                        if(mainBoard[row-1][col-1].getPiece() instanceof King)
                            collision=true;
                    }
                }
            }
        }else{
            if(row+1<8){
                if((mainBoard[row+1][col].getPiece() instanceof NonPiece))
                    controlBoard[row+1][col] = controlBoard[row][col];
                    coord = Integer.toString(row+1).concat(Integer.toString(col));
                    moves.add(coord);
                if(col+1<8){
                    if(!(mainBoard[row+1][col+1].getPiece() instanceof NonPiece) && mainBoard[row+1][col+1].getPiece().getTeam()!=white){
                        controlBoard[row+1][col+1] = controlBoard[row][col];
                        coord = Integer.toString(row+1).concat(Integer.toString(col+1));
                        moves.add(coord);
                        if(mainBoard[row+1][col+1].getPiece() instanceof King)
                            collision=true;
                    }
                }
                if(col-1>=0){
                    if(!(mainBoard[row+1][col-1].getPiece() instanceof NonPiece) && mainBoard[row+1][col-1].getPiece().getTeam()!=white){
                        controlBoard[row+1][col-1] = controlBoard[row][col];
                        coord = Integer.toString(row+1).concat(Integer.toString(col-1));
                        moves.add(coord);
                        if(mainBoard[row+1][col-1].getPiece() instanceof King)
                            collision=true;
                    }
                }
            }
        }
        return moves;
    }
}
