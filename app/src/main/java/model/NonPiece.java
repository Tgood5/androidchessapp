package model;

import java.util.ArrayList;

public class NonPiece extends Piece {

    public String name;

    public String getName(){
        return name;
    }

    public Square[][] move(String coordinates, Square[][] testBoard, int turnCount, Square[][] clearLayout){
        return testBoard;
    }

    public boolean getTeam(){
        return false;
    }

    @Override
    public ArrayList<String> populateTrajectory(int row, int col, Square[][] mainBoard, int[][] controlBoard) {
        ArrayList<String> moves = new ArrayList<>();
        return moves;
    }
}
