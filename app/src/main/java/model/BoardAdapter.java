package model;

import android.content.Context;
import android.graphics.Color;
import android.content.ClipData;
import android.net.Uri;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.util.Log;

import com.example.tgood.androidchess76.PlayActivity;
import com.example.tgood.androidchess76.R;

public class BoardAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private Square[][] board;

    Piece currentPiece = null;
    GridView gridView;

    // Constructor
    public BoardAdapter(Context c, Square[][] board) {
        mContext = c;
        Context context = c.getApplicationContext();
        mInflater = LayoutInflater.from(context);
        this.board = board;

    }


    public int getCount() {
        return 64;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView imageView;

        if(convertView == null) {
            imageView = new ImageView(mContext);
            int size = parent.getWidth()/8;
            imageView.setLayoutParams(new GridView.LayoutParams(size, size));
            int column = (position / 8) % 2;

            if (column == 0) {
                if (position % 2 == 0) {
                    imageView.setBackgroundColor(Color.parseColor("#ffffff"));
                } else {
                    imageView.setBackgroundColor(Color.parseColor("#808080"));
                }
            } else {

                if (position % 2 == 0) {
                    imageView.setBackgroundColor(Color.parseColor("#808080"));
                } else {
                    imageView.setBackgroundColor(Color.parseColor("#ffffff"));
                }
            }

            Piece piece = board[position/8][position%8].getPiece();

            if (piece != null) {
                String name = piece.getName();
                int imageID = mContext.getResources().getIdentifier(name, "drawable", mContext.getPackageName());

                if (name.compareTo("blank") == 0) {//blank piece

                    imageView.setImageResource(imageID);
                    imageView.setAlpha(0);
                } else {
                    imageView.setImageResource(imageID);
                }
            } else {
                imageView = (ImageView) convertView;
            }
        }else{

            imageView = (ImageView) convertView;
        }

        gridView = (GridView)parent;
        return imageView;
    }

    public void setGridListener() {
        gridView.setOnTouchListener(new OnTouchListener(){
            public boolean onTouch(View v, MotionEvent me){
                int action = me.getActionMasked();
                float xpos = me.getX();
                float ypos = me.getY();
                int position = gridView.pointToPosition((int)xpos, (int)ypos);
                if (action==MotionEvent.ACTION_UP){
                    //select piece at position
                    Log.v("Test", "touch event!!!");

                    return true;
                }
                return false;
            }
        });
    }
}
