package model;

import android.widget.ImageView;

import java.util.ArrayList;

public class Knight extends Piece {

    public String name;
    public boolean white;
    public ImageView figImage;

    public String getName(){
        return name;
    }

    public Square[][] move(String coordinates, Square[][] testBoard, int turnCount, Square[][] clearLayout){
        //parse input string
        String initialPosition = coordinates.substring(0, 2);
        String newPosition = coordinates.substring(3);
        int oldCol = (int)(initialPosition.charAt(0)-97);
        int newCol = (int)(newPosition.charAt(0)-97);
        int oldRow = 8-(int)(initialPosition.charAt(1)-48);
        int newRow = 8-(int)(newPosition.charAt(1)-48);

        boolean legal=false;
        Square[][] board = testBoard;
        //check out of bounds
        if(newCol > 7 || newCol < 0 || newRow > 7 || newRow < 0)
            return board;
        //check if new space is open or attacking
        if(white){
            if(!(board[newRow][newCol].getPiece() instanceof NonPiece)){
                if(board[newRow][newCol].getPiece().getTeam())
                    return board;
            }
        }else{
            if(!(board[newRow][newCol].getPiece() instanceof NonPiece)){
                if(!(board[newRow][newCol].getPiece().getTeam()))
                    return board;
            }
        }
        //moving, can jump other pieces
        if(newCol==oldCol+2 || newCol==oldCol-2){
            if(newRow==oldRow+1 || newRow==oldRow-1){
                legal=true;
            }
        }else if(newRow==oldRow+2 || newRow==oldRow-2){
            if(newCol==oldCol+1 || newCol==oldCol-1){
                legal=true;
            }
        }

        if(legal){							//if the move was legal, move the piece, otherwise throw error
            board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
            board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
        }

        return board;
    }

    public boolean getTeam(){
        return white;
    }

    @Override
    public ArrayList<String> populateTrajectory(int row, int col, Square[][] mainBoard, int[][] controlBoard) {
        //dont populate trajectory if takeable by defenders
        ArrayList<String> moves = new ArrayList<>();
        String coord;
        if(white){
            if(controlBoard[row][col]==2)
                return moves;
        }else{
            if(controlBoard[row][col]==1)
                return moves;
        }
        boolean collision = false;

        //update threat squares if not blocked, not out of bounds
        //return true if threatening enemy king
        if(row+2<8){
            if(col+1<8){
                controlBoard[row+2][col+1] = controlBoard[row][col];
                if(mainBoard[row+2][col+1].getPiece() instanceof NonPiece) {
                    coord = Integer.toString(row + 2).concat(Integer.toString(col + 1));
                    moves.add(coord);
                }
                if(mainBoard[row+2][col+1].getPiece() instanceof King && mainBoard[row+2][col+1].getPiece().getTeam()!=white)
                    collision=true;
            }
            if(col-1>=0){
                controlBoard[row+2][col-1] = controlBoard[row][col];
                if(mainBoard[row+2][col-1].getPiece() instanceof NonPiece) {
                    coord = Integer.toString(row + 2).concat(Integer.toString(col - 1));
                    moves.add(coord);
                }
                if(mainBoard[row+2][col-1].getPiece() instanceof King && mainBoard[row+2][col-1].getPiece().getTeam()!=white)
                    collision=true;
            }
        }
        if(row-2>=0){
            if(col+1<8){
                controlBoard[row-2][col+1] = controlBoard[row][col];
                if(mainBoard[row-2][col+1].getPiece() instanceof NonPiece) {
                    coord = Integer.toString(row - 2).concat(Integer.toString(col + 1));
                    moves.add(coord);
                }
                if(mainBoard[row-2][col+1].getPiece() instanceof King && mainBoard[row-2][col+1].getPiece().getTeam()!=white)
                    collision=true;
            }
            if(col-1>=0){
                controlBoard[row-2][col-1] = controlBoard[row][col];
                if(mainBoard[row-2][col-1].getPiece() instanceof NonPiece) {
                    coord = Integer.toString(row - 2).concat(Integer.toString(col - 1));
                    moves.add(coord);
                }
                if(mainBoard[row-2][col-1].getPiece() instanceof King && mainBoard[row-2][col-1].getPiece().getTeam()!=white)
                    collision=true;
            }
        }
        if(row+1<8){
            if(col+2<8){
                controlBoard[row+1][col+2] = controlBoard[row][col];
                if(mainBoard[row+1][col+2].getPiece() instanceof NonPiece) {
                    coord = Integer.toString(row + 1).concat(Integer.toString(col + 2));
                    moves.add(coord);
                }
                if(mainBoard[row+1][col+2].getPiece() instanceof King && mainBoard[row+1][col+2].getPiece().getTeam()!=white)
                    collision=true;
            }
            if(col-2>=0){
                controlBoard[row+1][col-2] = controlBoard[row][col];
                if(mainBoard[row+1][col-2].getPiece() instanceof NonPiece) {
                    coord = Integer.toString(row + 1).concat(Integer.toString(col - 2));
                    moves.add(coord);
                }
                if(mainBoard[row+1][col-2].getPiece() instanceof King && mainBoard[row+1][col-2].getPiece().getTeam()!=white)
                    collision=true;
            }
        }
        if(row-1>=0){
            if(col+2<8){
                controlBoard[row-1][col+2] = controlBoard[row][col];
                if(mainBoard[row-1][col+2].getPiece() instanceof NonPiece) {
                    coord = Integer.toString(row - 1).concat(Integer.toString(col + 2));
                    moves.add(coord);
                }
                if(mainBoard[row-1][col+2].getPiece() instanceof King && mainBoard[row-1][col+2].getPiece().getTeam()!=white)
                    collision=true;
            }
            if(col-2>=0){
                controlBoard[row-1][col-2] = controlBoard[row][col];
                if(mainBoard[row-1][col-2].getPiece() instanceof NonPiece) {
                    coord = Integer.toString(row - 1).concat(Integer.toString(col - 2));
                    moves.add(coord);
                }
                if(mainBoard[row-1][col-2].getPiece() instanceof King && mainBoard[row-1][col-2].getPiece().getTeam()!=white)
                    collision=true;
            }
        }

        return moves;
    }
}
