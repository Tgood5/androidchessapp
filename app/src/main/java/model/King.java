package model;

import android.widget.ImageView;

import java.util.ArrayList;

public class King extends Piece {

    public String name;
    public boolean white;
    public ImageView figImage;
    public boolean canCastle=true;

    public String getName(){
        return name;
    }

    public Square[][] move(String coordinates, Square[][] testBoard, int turnCount, Square[][] clearLayout){
        //parse input string
        String initialPosition = coordinates.substring(0, 2);
        String newPosition = coordinates.substring(3);
        int oldCol = (int)(initialPosition.charAt(0)-97);
        int newCol = (int)(newPosition.charAt(0)-97);
        int oldRow = 8-(int)(initialPosition.charAt(1)-48);
        int newRow = 8-(int)(newPosition.charAt(1)-48);

        Square[][] board = testBoard;
        boolean legal=false;
        //check out of bounds
        if(newCol > 7 || newCol < 0 || newRow > 7 || newRow < 0)
            return board;
        //check if new space is open or attacking
        if(white){
            if(!(board[newRow][newCol].getPiece() instanceof NonPiece)){
                if(board[newRow][newCol].getPiece().getTeam())
                    return board;
            }
        }else{
            if(!(board[newRow][newCol].getPiece() instanceof NonPiece)){
                if(!(board[newRow][newCol].getPiece().getTeam()))
                    return board;
            }
        }
        if(canCastle){						//CASTLING
            if(newRow==oldRow && newCol==oldCol+2){
                //castle right
                if(board[oldRow][oldCol+3].getPiece() instanceof Rook
                        && board[oldRow][oldCol+1].getPiece() instanceof NonPiece){
                    Rook tmp = (Rook) board[oldRow][oldCol+3].getPiece();
                    if (tmp.canCastle){
                        board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
                        board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
                        board[newRow][newCol-1].setPiece(board[oldRow][oldCol+3].getPiece());
                        board[oldRow][oldCol+3].setPiece(clearLayout[oldRow][oldCol+3].getPiece());
                        return board;
                    }
                }
            }else if(newRow==oldRow && newCol==oldCol-2){
                //castle left
                if(board[oldRow][oldCol-4].getPiece() instanceof Rook
                        && board[oldRow][oldCol-1].getPiece() instanceof NonPiece
                        && board[oldRow][oldCol-3].getPiece() instanceof NonPiece){
                    Rook tmp = (Rook) board[oldRow][oldCol-4].getPiece();
                    if (tmp.canCastle){
                        board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
                        board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
                        board[newRow][newCol+1].setPiece(board[oldRow][oldCol-4].getPiece());
                        board[oldRow][oldCol-4].setPiece(clearLayout[oldRow][oldCol-4].getPiece());
                        return board;
                    }
                }
            }
        }//END CASTLING

        //can move 1 space in any direction
        if(newCol==oldCol+1 || newCol==oldCol-1){
            if(newRow==oldRow || newRow==oldRow+1 || newRow==oldRow-1){
                legal=true;
            }
        }else if(newRow==oldRow+1 || newRow==oldRow-1){
            if(newCol==oldCol || newCol==oldCol+1 || newRow==oldCol-1){
                legal=true;
            }
        }

        if(legal){							//if the move was legal, move the piece, otherwise throw error
            board[newRow][newCol].setPiece(board[oldRow][oldCol].getPiece());
            board[oldRow][oldCol].setPiece(clearLayout[oldRow][oldCol].getPiece());
            canCastle=false;
        }
        return board;
    }

    public boolean getTeam(){
        return white;
    }

    @Override
    public ArrayList<String> populateTrajectory(int row, int col, Square[][] mainBoard, int[][] controlBoard) {
        int teamnum;
        String coord;
        ArrayList<String> moves = new ArrayList<>();
        if(white)
            teamnum=1;
        else
            teamnum=2;
        //project on enemies in range
        if((row-1>=0) && (col-1>=0)){
            if(mainBoard[row-1][col-1].getPiece() instanceof NonPiece && controlBoard[row-1][col-1]==teamnum || controlBoard[row-1][col-1]==0){
                coord = Integer.toString(row-1).concat(Integer.toString(col-1));
                moves.add(coord);
            }
            if(!(mainBoard[row-1][col-1].getPiece() instanceof NonPiece) && mainBoard[row-1][col-1].getPiece().getTeam()!=white){
                controlBoard[row-1][col-1]=teamnum;
            }
        }else if(row -1 >= 0){
            if(mainBoard[row-1][col].getPiece() instanceof NonPiece && controlBoard[row-1][col]==teamnum || controlBoard[row-1][col]==0){
                coord = Integer.toString(row-1).concat(Integer.toString(col));
                moves.add(coord);
            }
            if(!(mainBoard[row-1][col].getPiece() instanceof NonPiece) && mainBoard[row-1][col].getPiece().getTeam()!=white){
                controlBoard[row-1][col]=teamnum;
            }
        }else if(row - 1 >= 0 && col + 1 < 8){
            if(mainBoard[row-1][col+1].getPiece() instanceof NonPiece && controlBoard[row-1][col+1]==teamnum || controlBoard[row-1][col+1]==0){
                coord = Integer.toString(row-1).concat(Integer.toString(col+1));
                moves.add(coord);
            }
            if(!(mainBoard[row-1][col+1].getPiece() instanceof NonPiece) && mainBoard[row-1][col+1].getPiece().getTeam()!=white){
                controlBoard[row-1][col+1]=teamnum;
            }
        }else if(col + 1 < 8){
            if(mainBoard[row][col-1].getPiece() instanceof NonPiece && controlBoard[row][col-1]==teamnum || controlBoard[row][col-1]==0){
                coord = Integer.toString(row-1).concat(Integer.toString(col-1));
                moves.add(coord);
            }
            if(!(mainBoard[row][col+1].getPiece() instanceof NonPiece) && mainBoard[row][col+1].getPiece().getTeam()!=white){
                controlBoard[row][col+1]=teamnum;
            }
        }else if(row + 1 < 8 && col + 1 < 8){
            if(mainBoard[row+1][col+1].getPiece() instanceof NonPiece && controlBoard[row+1][col+1]==teamnum || controlBoard[row+1][col+1]==0){
                coord = Integer.toString(row+1).concat(Integer.toString(col+1));
                moves.add(coord);
            }
            if(!(mainBoard[row+1][col+1].getPiece() instanceof NonPiece) && mainBoard[row+1][col+1].getPiece().getTeam()!=white){
                controlBoard[row+1][col+1]=teamnum;
            }
        }else if(row + 1 < 8){
            if(mainBoard[row+1][col].getPiece() instanceof NonPiece && controlBoard[row+1][col]==teamnum || controlBoard[row+1][col]==0){
                coord = Integer.toString(row+1).concat(Integer.toString(col));
                moves.add(coord);
            }
            if(!(mainBoard[row+1][col].getPiece() instanceof NonPiece) && mainBoard[row+1][col].getPiece().getTeam()!=white){
                controlBoard[row+1][col]=teamnum;
            }
        }else if(row + 1 < 8 && col - 1 >= 0){
            if(mainBoard[row+1][col-1].getPiece() instanceof NonPiece && controlBoard[row+1][col-1]==teamnum || controlBoard[row+1][col-1]==0){
                coord = Integer.toString(row+1).concat(Integer.toString(col-1));
                moves.add(coord);
            }
            if(!(mainBoard[row+1][col-1].getPiece() instanceof NonPiece) && mainBoard[row+1][col-1].getPiece().getTeam()!=white){
                controlBoard[row+1][col-1]=teamnum;
            }
        }else if(col -1 >= 0){
            if(mainBoard[row][col-1].getPiece() instanceof NonPiece && controlBoard[row][col-1]==teamnum || controlBoard[row][col-1]==0){
                coord = Integer.toString(row).concat(Integer.toString(col-1));
                moves.add(coord);
            }
            if(!(mainBoard[row][col-1].getPiece() instanceof NonPiece) && mainBoard[row][col-1].getPiece().getTeam()!=white){
                controlBoard[row][col-1]=teamnum;
            }
        }
        return moves;
    }
}
