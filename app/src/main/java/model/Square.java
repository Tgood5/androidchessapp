package model;

import java.io.Serializable;

public class Square implements Serializable {

    private static final long serialVersionUID = 1L;
    private int rank;
    private int file;
    private Piece piece;

    public Square(int rank, int file){
        this.rank = rank;
        this.file = file;
    }

    public int getRank(){
        return rank;
    }

    public int getFile(){
        return file;
    }

    public void setRank(int rank){
        this.rank = rank;
    }

    public void setFile(int file){
        this.file = file;
    }

    public Piece getPiece(){
        return piece;
    }

    public void setPiece(Piece piece){
        this.piece = piece;
    }

}
