package model;

import java.util.ArrayList;
import java.util.StringTokenizer;

public abstract class Piece {

    private Square[][] board;
    public boolean white;

    public abstract String getName();

    public void setBoard(Square[][] board){
        this.board = board;
    }

    public Square[][] getBoard(){
        return board;
    }

    public abstract boolean getTeam();

    public abstract Square[][] move(String coordinates, Square[][] board, int turnCount, Square[][] clearLayout);

    public abstract ArrayList<String> populateTrajectory(int row, int col, Square[][] mainBoard, int[][] controlBoard);
}

