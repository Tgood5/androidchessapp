package com.example.tgood.androidchess76;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.v7.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    //Called when the User selects "Play"
    public void startGame(View view){
        Intent intent = new Intent(this, PlayActivity.class);
        MainActivity.this.startActivity(intent);
    }

    //Called when the User selects "Record"
    public void recordGame(View view){
        Intent intent = new Intent(this, RecordActivity.class);
        MainActivity.this.startActivity(intent);
    }

    //Called when the User selects "Playback"
    public void playBack(View view){
        Intent intent = new Intent(this, PlaybackActivity.class);
        MainActivity.this.startActivity(intent);
    }
}
