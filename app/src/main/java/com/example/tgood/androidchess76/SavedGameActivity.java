package com.example.tgood.androidchess76;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import control.GamePlay;
import model.BoardAdapter;
import model.King;
import model.NonPiece;
import model.Square;

public class SavedGameActivity extends Activity implements AdapterView.OnItemClickListener {

    private GamePlay game;
    private GridView board;
    Button forwardButton;
    Button backButton;
    private LayoutInflater layoutInflater;
    private RelativeLayout rootLayout;
    private TextView gameMessage;
    private int moveIndex;
    private BoardAdapter adapter;
    private static boolean newgame = true;
    private ArrayList<String> chosenGame;
    private View[] selectedViews;
    private Square[] selectedSquares;
    private String[] moveCoordinates;
    private Square[][] undoBoard = new Square[8][8];
    private boolean drawRequest=false;
    private ArrayList<String> reverseCoordinates;
    private int[] RankFile; //0 = initial rank, 1 = initial file, 2 = move rank, 3 = move file
    private final String[] coordinates = new String[]{
            "a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8",
            "a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7",
            "a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6",
            "a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5",
            "a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4",
            "a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3",
            "a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2",
            "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1",};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_game);

        if(newgame){
            this.game = new GamePlay();
            game.setTurnCount(1);
            adapter = new BoardAdapter(this, game.getBoard());
            selectedViews = new View[2];
            moveCoordinates = new String[2];
            selectedSquares = new Square[2];
            moveIndex = 1;
            RankFile = new int[4];
            chosenGame =  (ArrayList<String>)getIntent().getSerializableExtra("FILES_TO_SEND");
            reverseCoordinates = new ArrayList<String>();
            //For easy lookup of position:
            reverseCoordinates.add(0, "a8");
            reverseCoordinates.add(1, "b8");
            reverseCoordinates.add(2, "c8");
            reverseCoordinates.add(3, "d8");
            reverseCoordinates.add(4, "e8");
            reverseCoordinates.add(5, "f8");
            reverseCoordinates.add(6, "g8");
            reverseCoordinates.add(7, "h8");
            reverseCoordinates.add(8, "a7");
            reverseCoordinates.add(9, "b7");
            reverseCoordinates.add(10, "c7");
            reverseCoordinates.add(11, "d7");
            reverseCoordinates.add(12, "e7");
            reverseCoordinates.add(13, "f7");
            reverseCoordinates.add(14, "g7");
            reverseCoordinates.add(15, "h7");
            reverseCoordinates.add(16, "a6");
            reverseCoordinates.add(17, "b6");
            reverseCoordinates.add(18, "c6");
            reverseCoordinates.add(19, "d6");
            reverseCoordinates.add(20, "e6");
            reverseCoordinates.add(21, "f6");
            reverseCoordinates.add(22, "g6");
            reverseCoordinates.add(23, "h6");
            reverseCoordinates.add(24, "a5");
            reverseCoordinates.add(25, "b5");
            reverseCoordinates.add(26, "c5");
            reverseCoordinates.add(27, "d5");
            reverseCoordinates.add(28, "e5");
            reverseCoordinates.add(29, "f5");
            reverseCoordinates.add(30, "g5");
            reverseCoordinates.add(31, "h5");
            reverseCoordinates.add(32, "a4");
            reverseCoordinates.add(33, "b4");
            reverseCoordinates.add(34, "c4");
            reverseCoordinates.add(35, "d4");
            reverseCoordinates.add(36, "e4");
            reverseCoordinates.add(37, "f4");
            reverseCoordinates.add(38, "g4");
            reverseCoordinates.add(39, "h4");
            reverseCoordinates.add(40, "a3");
            reverseCoordinates.add(41, "b3");
            reverseCoordinates.add(42, "c3");
            reverseCoordinates.add(43, "d3");
            reverseCoordinates.add(44, "e3");
            reverseCoordinates.add(45, "f3");
            reverseCoordinates.add(46, "g3");
            reverseCoordinates.add(47, "h3");
            reverseCoordinates.add(48, "a2");
            reverseCoordinates.add(49, "b2");
            reverseCoordinates.add(50, "c2");
            reverseCoordinates.add(51, "d2");
            reverseCoordinates.add(52, "e2");
            reverseCoordinates.add(53, "f2");
            reverseCoordinates.add(54, "g2");
            reverseCoordinates.add(55, "h2");
            reverseCoordinates.add(56, "a1");
            reverseCoordinates.add(57, "b1");
            reverseCoordinates.add(58, "c1");
            reverseCoordinates.add(59, "d1");
            reverseCoordinates.add(60, "e1");
            reverseCoordinates.add(61, "f1");
            reverseCoordinates.add(62, "g1");
            reverseCoordinates.add(63, "h1");
            newgame = false;
        }

        initializeBack();
        initializeForward();

        rootLayout = new RelativeLayout(getApplicationContext());
        GridView gridview = (GridView) findViewById(R.id.gridview2);
        gridview.setAdapter(adapter);
        gridview.setOnItemClickListener(this);
        this.board = gridview;
        TextView text = (TextView) findViewById(R.id.message2);
        this.gameMessage = text;
        this.forwardButton = (Button)findViewById(R.id.forward);
        this.backButton = (Button)findViewById(R.id.back);
        undoBoard = game.getBoard();


    }

    private void initializeBack(){

        Button backButton = (Button) findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View argo){
                back();
            }
        });

    }

    private void initializeForward(){

        Button forwardButton = (Button) findViewById(R.id.forward);
        forwardButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View argo){
                executeMove();
            }
        });

    }



    private void back(){
        if(undoBoard==game.getBoard()) {
            gameMessage.setText("Can't go back.");
            return;
        }
        this.game.setBoard(undoBoard);
        adapter = new BoardAdapter(this, undoBoard);
        board.setAdapter(adapter);
        if(moveIndex > 0) {
            moveIndex--;
        }
        game.setTurnCount(game.getTurnCount()-1);
        gameMessage.setText("Back pressed.");
    }

    public void executeMove(){

        //Making sure that there are still moves available in the game:
        if(chosenGame.size() == moveIndex){
            gameMessage.setText("No more moves left! This game is over.");
            return;
        }

        if(moveIndex < 0){
            return;
        }

        //Simplified version of PlayActivity:

        forwardButton.setText("forward");
        backButton.setText("back");
        int column = -1;
        String moveResult;
        boolean legal = true;
        drawRequest=false;
        Square[][] prevTurn = new Square[8][8];

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Square newSquare = new Square(i, j);
                newSquare.setPiece(game.getBoard()[i][j].getPiece());
                prevTurn[i][j] = newSquare;
            }
        }

        String coordinate = chosenGame.get(moveIndex);

        if(coordinate != null && coordinate.compareTo("") != 0){

            //Attempting to execute the move:

            //Populating selectedSquares:
            String initialCoordinates = coordinate.substring(0,2);
            String destinationCoordinates = coordinate.substring(3);

            //System.out.println("initialCoordinates:" + initialCoordinates+"!!");
            //System.out.println("distanationCoordinates:" + destinationCoordinates+"!!");

            int initialPosition = reverseCoordinates.indexOf(initialCoordinates);
            int movePosition = reverseCoordinates.indexOf(destinationCoordinates);

            //System.out.println("initialPOs: " + initialPosition);
            //System.out.println("movePos: " + movePosition);

            undoBoard = prevTurn;

            Square selectedSquare1 = game.getBoard()[initialPosition/8][initialPosition % 8];
            Square selectedSquare2 = game.getBoard()[movePosition/8][movePosition % 8];

            moveCoordinates[0] = initialCoordinates;
            selectedSquares[0] = selectedSquare1;
            selectedSquares[1] = selectedSquare2;
            RankFile[0] = selectedSquares[0].getRank();
            RankFile[1] = selectedSquares[0].getFile();
            RankFile[2] = selectedSquares[1].getRank();
            RankFile[3] = selectedSquares[1].getFile();

            Square[][] oldBoard = game.getBoard();
            Square[][] savedBoard = oldBoard;
            Square[][] newBoard = selectedSquares[0].getPiece().move(coordinate, oldBoard, game.getTurnCount(), game.getClearLayout());

            if(newBoard[RankFile[0]][RankFile[1]].getPiece() instanceof NonPiece){
                //Legal movement, updating the board:
                NonPiece newPiece = new NonPiece();
                newPiece.name = "blank";

                Square newSquare = new Square(RankFile[0], RankFile[1]);
                newSquare.setPiece(newPiece);
                newBoard[RankFile[0]][RankFile[1]] = newSquare;
                game.setBoard(newBoard);
                this.adapter.notifyDataSetChanged();
                this.board.setAdapter(adapter);
                //update turn count
                game.setTurnCount(game.getTurnCount()+1);
                gameMessage.setText("Sucessful move!");
            }else{
                //Illegal move:
                gameMessage.setText("Illegal move! Try again.");

            }
            //if movement was legal, determine game status------------------------
            moveResult=isKingChecked(game.getBoard());
            //System.out.println(moveResult);
            switch(moveResult){
                case "blackChecked"	:
                    //illegal if black is left in check as a result of own move
                    if(game.getTurnCount()%2 == 1){
                        legal=false;
                        break;
                    }
                    gameMessage.setText("Black is in Check");
                    break;

                case "whiteChecked" :
                    //illegal if white is left in check as a result of own move
                    if(game.getTurnCount()%2 == 0){
                        legal=false;
                        break;
                    }
                    gameMessage.setText("White is in Check");
                    break;

                case "bothChecked"	:
                    //illegal move
                    legal=false;
                    break;
                case "blackCheckMate" :
                    if(game.getTurnCount()%2 == 1){
                        legal=false;
                        break;
                    }
                    //black is checkmated, white wins
                    gameMessage.setText("White wins");
                    return;
                case "whiteCheckMate" :
                    if(game.getTurnCount()%2 == 0){
                        legal=false;
                        break;
                    }
                    //white is checkmated, white wins
                    gameMessage.setText("Black wins");
                    //endGame();
                    return;
                case "StaleMate"	:
                    //Stalemate...
                    System.out.println("Stalemate");
                    //endGame();
                    return;

                default:
                    //continue normally if no conditions are met
                    break;

            }
            //undo move if illegal
            if(!legal){
                this.game.setBoard(prevTurn);
                //this.adapter.notifyDataSetChanged();
                //this.board.setAdapter(adapter);
                adapter = new BoardAdapter(this, prevTurn);
                board.setAdapter(adapter);
                game.setTurnCount(game.getTurnCount()-1);
                if(moveIndex > 0) {
                    moveIndex--;
                }
                gameMessage.setText("Illegal move, king in check.");
            }

            moveIndex++;
            legal = true;
            column = -1;
            selectedViews[0] = null;
            selectedViews[1] = null;
            moveCoordinates[0] = null;
            moveCoordinates[1] = null;

        }else{

            gameMessage.setText("Error");
        }
    }

    public String isKingChecked(Square[][] mainBoard){

        int[][] controlBoard = new int[8][8];
        int blackRow = -1;
        int blackCol = -1;
        int whiteRow = -1;
        int whiteCol = -1;
        int whiteAttacker = 0;
        int blackAttacker = 0;
        int blackCollisionNum = 0;
        int whiteCollisionNum = 0;
        ArrayList<String> whiteCollision = new ArrayList<>();
        ArrayList<String> blackCollision = new ArrayList<>();
        boolean whiteChecked = false;
        boolean blackChecked = false;
        boolean whiteCanEscape = false;
        boolean blackCanEscape = false;
        boolean whiteCheckMate = false;
        boolean blackCheckMate = false;
        boolean blackStillHit = false;
        boolean whiteStillHit = false;
        int xtran;
        int ytran;

        //Copying mainBoard's contents to controlBoard; 0 = no piece, 1 = white, 2 = black

        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){

                if(mainBoard[i][j].getPiece() instanceof NonPiece){
                    //No piece
                    controlBoard[i][j] = 0;
                }else{
                    if(mainBoard[i][j].getPiece().getTeam() == false){
                        if(mainBoard[i][j].getPiece() instanceof King){
                            //Black King position
                            blackRow = i;
                            blackCol = j;
                            controlBoard[i][j] = 2;
                        }else{
                            //Black piece
                            controlBoard[i][j] = 2;
                        }
                    }else if(mainBoard[i][j].getPiece().getTeam() == true){
                        if(mainBoard[i][j].getPiece() instanceof King){
                            //White King position
                            whiteRow = i;
                            whiteCol = j;
                            controlBoard[i][j] = 1;
                        }else{
                            //White piece
                            controlBoard[i][j] = 1;
                        }
                    }
                }
            }
        }

        //Making Sure both Kings can be found on the board:

        if(blackRow == -1 && blackCol == -1){
            return "White wins";
        }else if(whiteRow == -1 && whiteCol == -1){
            return "Black wins";
        }

        //----------------CHECKING WHITE KING----------------

        //projecting black pieces:
        for(int p = 0; p < 8; p++){
            for (int q = 0; q < 8; q++){
                //Identifying black piece:
                if(mainBoard[p][q].getPiece().getTeam() == false){
                    //Projecting black piece:
                    whiteCollision = mainBoard[p][q].getPiece().populateTrajectory(p,  q,  mainBoard, controlBoard);
                    for(int i=0; i<whiteCollision.size(); i++){
                        xtran=whiteCollision.get(i).charAt(0)-'0';
                        ytran=whiteCollision.get(i).charAt(1)-'0';
                        if(mainBoard[xtran][ytran].getPiece() instanceof King && mainBoard[xtran][ytran].getPiece().getTeam())
                            whiteCollisionNum++;
                    }
                }
            }
        }

        //Checking if any of the black pieces can reach the WHITE KING:

        if(whiteCollisionNum > 0)
            whiteChecked=true;

        //Checking if the WHITE KING can escape:

        if(whiteChecked == true){

            if((whiteRow - 1 >= 0) && (whiteCol - 1>=0)){
                if(controlBoard[whiteRow-1][whiteCol-1] != 2
                        && mainBoard[whiteRow-1][whiteCol-1].getPiece() instanceof NonPiece){
                    whiteCanEscape = true;
                }
            }
            if(whiteRow -1 >= 0){
                if(controlBoard[whiteRow-1][whiteCol] != 2
                        && mainBoard[whiteRow-1][whiteCol].getPiece() instanceof NonPiece){
                    whiteCanEscape = true;
                }
            }
            if(whiteRow - 1 >= 0 && whiteCol + 1 < 8){
                if(controlBoard[whiteRow-1][whiteCol+1] != 2
                        && mainBoard[whiteRow-1][whiteCol+1].getPiece() instanceof NonPiece){
                    whiteCanEscape = true;
                }
            }
            if(whiteCol + 1 < 8){
                if(controlBoard[whiteRow][whiteCol+1] != 2
                        && mainBoard[whiteRow][whiteCol+1].getPiece() instanceof NonPiece){
                    whiteCanEscape = true;
                }
            }
            if(whiteRow + 1 < 8 && whiteCol + 1 < 8){
                if(controlBoard[whiteRow+1][whiteCol+1] != 2
                        && mainBoard[whiteRow+1][whiteCol+1].getPiece() instanceof NonPiece){
                    whiteCanEscape = true;
                }
            }
            if(whiteRow + 1 < 8){
                if(controlBoard[whiteRow + 1][whiteCol] != 2
                        && mainBoard[whiteRow+1][whiteCol].getPiece() instanceof NonPiece){
                    whiteCanEscape = true;
                }
            }
            if(whiteRow + 1 < 8 && whiteCol - 1 >= 0){
                if(controlBoard[whiteRow+1][whiteCol-1] != 2
                        && mainBoard[whiteRow+1][whiteCol-1].getPiece() instanceof NonPiece){
                    whiteCanEscape = true;
                }
            }
            if(whiteCol -1 >= 0){
                if(controlBoard[whiteRow][whiteCol-1] != 2
                        && mainBoard[whiteRow][whiteCol-1].getPiece() instanceof NonPiece){
                    whiteCanEscape = true;
                }
            }

        }

        //If WHITE KING is checked and can't escape, checking if other pieces can defend him:

        if(whiteChecked == true && whiteCanEscape == false){

            //Resetting the controlBoard; 0 = no piece, 1 = white, 2 = black

            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){

                    if(mainBoard[i][j].getPiece() instanceof NonPiece){
                        //No piece
                        controlBoard[i][j] = 0;
                    }else{
                        if(mainBoard[i][j].getPiece().getTeam() == false){
                            if(mainBoard[i][j].getPiece() instanceof King){
                                //Black King position
                                blackRow = i;
                                blackCol = j;
                                controlBoard[i][j] = 2;
                            }else{
                                //Black piece
                                controlBoard[i][j] = 2;
                            }
                        }else if(mainBoard[i][j].getPiece().getTeam() == true){
                            if(mainBoard[i][j].getPiece() instanceof King){
                                //White King position
                                whiteRow = i;
                                whiteCol = j;
                                controlBoard[i][j] = 1;
                            }else{
                                //White piece
                                controlBoard[i][j] = 1;
                            }
                        }
                    }
                }
            }

            //Projecting the WHITE defenders to simulate blocking:

            for(int m = 0; m < 8; m++){
                for(int n = 0; n < 8; n++){
                    //Identifying white piece:
                    if(mainBoard[m][n].getPiece().getTeam() == true){
                        //Projecting white piece:
                        mainBoard[m][n].getPiece().populateTrajectory(m, n, mainBoard, controlBoard);
                    }
                }
            }

            //Projecting the BLACK attackers to see if white king is still hit:

            for(int m = 0; m < 8; m++){
                for(int n = 0; n < 8; n++){
                    //Identifying black piece:
                    if(mainBoard[m][n].getPiece().getTeam() == false){
                        //Projecting black piece:
                        whiteCollision = mainBoard[m][n].getPiece().populateTrajectory(m, n, mainBoard, controlBoard);
                        for(int i=0; i<whiteCollision.size(); i++){
                            xtran=whiteCollision.get(i).charAt(0)-'0';
                            ytran=whiteCollision.get(i).charAt(1)-'0';
                            if(mainBoard[xtran][ytran].getPiece() instanceof King && mainBoard[xtran][ytran].getPiece().getTeam())
                                whiteStillHit=true;
                        }
                    }
                }
            }

            //Checking if the WHITE KING is still hit:

            if(whiteStillHit)
                whiteCheckMate=true;

            //If the WHITE KING is not hit, however the number of collisions was 2 or higher, we still have a checkmate:

            if(whiteCheckMate == false && whiteCollisionNum > 1){
                whiteCheckMate = true;
            }
        }

        //----------------CHECKING BLACK KING----------------

        //Resetting the controlBoard; 0 = no piece, 1 = white, 2 = black

        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){

                if(mainBoard[i][j].getPiece() instanceof NonPiece){
                    //No piece
                    controlBoard[i][j] = 0;
                }else{
                    if(mainBoard[i][j].getPiece().getTeam() == false){
                        if(mainBoard[i][j].getPiece() instanceof King){
                            //Black King position
                            blackRow = i;
                            blackCol = j;
                            controlBoard[i][j] = 2;
                        }else{
                            //Black piece
                            controlBoard[i][j] = 2;
                        }
                    }else if(mainBoard[i][j].getPiece().getTeam() == true){
                        if(mainBoard[i][j].getPiece() instanceof King){
                            //White King position
                            whiteRow = i;
                            whiteCol = j;
                            controlBoard[i][j] = 1;
                        }else{
                            //White piece
                            controlBoard[i][j] = 1;
                        }
                    }
                }
            }
        }

        //Checking whether the BLACK KING is checked:

        //projecting white pieces:
        for(int p = 0; p < 8; p++){
            for (int q = 0; q < 8; q++){
                //Identifying white piece:
                if(mainBoard[p][q].getPiece().getTeam() == true){
                    //Projecting white piece:
                    blackCollision = mainBoard[p][q].getPiece().populateTrajectory(p,  q,  mainBoard, controlBoard);
                    for(int i=0; i<blackCollision.size(); i++){
                        xtran=blackCollision.get(i).charAt(0)-'0';
                        ytran=blackCollision.get(i).charAt(1)-'0';
                        if(mainBoard[xtran][ytran].getPiece() instanceof King && mainBoard[xtran][ytran].getPiece().getTeam()==false)
                            blackCollisionNum++;
                    }
                }
            }
        }
        //TESTING
        //System.out.println("white attackers: " + blackCollisionNum +"");

        //Checking if any of the white pieces can reach the BLACK KING:
        if(blackCollisionNum>0){
            blackChecked = true;
        }
        //Checking if the BLACK KING can escape:
        if(blackChecked == true){

            if((blackRow - 1 >= 0) && (blackCol - 1>=0)){
                if(controlBoard[blackRow-1][blackCol-1] != 1
                        && (mainBoard[blackRow-1][blackCol-1].getPiece() instanceof NonPiece)){
                    blackCanEscape = true;
                }
            }
            if(blackRow -1 >= 0){
                if(controlBoard[blackRow-1][blackCol] != 1
                        && (mainBoard[blackRow-1][blackCol].getPiece() instanceof NonPiece)){
                    blackCanEscape = true;
                }
            }
            if(blackRow - 1 >= 0 && blackCol + 1 < 8){
                if(controlBoard[blackRow-1][blackCol+1] != 1
                        && (mainBoard[blackRow-1][blackCol+1].getPiece() instanceof NonPiece)){
                    blackCanEscape = true;
                }
            }
            if(blackCol + 1 < 8){
                if(controlBoard[blackRow][blackCol+1] != 1
                        && (mainBoard[blackRow][blackCol+1].getPiece() instanceof NonPiece)){
                    blackCanEscape = true;
                }
            }
            if(blackRow + 1 < 8 && blackCol + 1 < 8){
                if(controlBoard[blackRow+1][blackCol+1] != 1
                        && (mainBoard[blackRow+1][blackCol+1].getPiece() instanceof NonPiece)){
                    blackCanEscape = true;
                }
            }
            if(blackRow + 1 < 8){
                if(controlBoard[blackRow + 1][blackCol] != 1
                        && (mainBoard[blackRow+1][blackCol].getPiece() instanceof NonPiece)){
                    blackCanEscape = true;
                }
            }
            if(blackRow + 1 < 8 && blackCol - 1 >= 0){
                if(controlBoard[blackRow+1][blackCol-1] != 1
                        && (mainBoard[blackRow+1][blackCol-1].getPiece() instanceof NonPiece)){
                    blackCanEscape = true;
                }
            }
            if(blackCol - 1 >= 0){
                if(controlBoard[blackRow][blackCol-1] != 1
                        && (mainBoard[blackRow][blackCol-1].getPiece() instanceof NonPiece)){
                    blackCanEscape = true;
                }
            }

        }

        //If BLACK KING is checked and can't escape, checking if other pieces can defend him:



        if(blackChecked == true && blackCanEscape == false){

            //Resetting the controlBoard; 0 = no piece, 1 = white, 2 = black
            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){

                    if(mainBoard[i][j].getPiece() instanceof NonPiece){
                        //No piece
                        controlBoard[i][j] = 0;
                    }else{
                        if(mainBoard[i][j].getPiece().getTeam() == false){
                            if(mainBoard[i][j].getPiece() instanceof King){
                                //Black King position
                                blackRow = i;
                                blackCol = j;
                                controlBoard[i][j] = 2;
                            }else{
                                //Black piece
                                controlBoard[i][j] = 2;
                            }
                        }else if(mainBoard[i][j].getPiece().getTeam() == true){
                            if(mainBoard[i][j].getPiece() instanceof King){
                                //White King position
                                whiteRow = i;
                                whiteCol = j;
                                controlBoard[i][j] = 1;
                            }else{
                                //White piece
                                controlBoard[i][j] = 1;
                            }
                        }
                    }
                }
            }

            //Projecting the BLACK defenders:
            for(int p = 0; p < 8; p++){
                for (int q = 0; q < 8; q++){
                    //Identifying black piece:
                    if(mainBoard[p][q].getPiece().getTeam() == false){
                        //Projecting black piece:
                        mainBoard[p][q].getPiece().populateTrajectory(p,  q,  mainBoard, controlBoard);
                    }
                }
            }

            //Projecting WHITE attackers:
            for(int m = 0; m < 8; m++){
                for(int n = 0; n < 8; n++){
                    //Identifying white piece:
                    if(mainBoard[m][n].getPiece().getTeam() == true){
                        //Projecting white piece:
                        blackCollision = mainBoard[m][n].getPiece().populateTrajectory(m, n, mainBoard, controlBoard);
                        for(int i=0; i<blackCollision.size(); i++){
                            xtran=blackCollision.get(i).charAt(0)-'0';
                            ytran=blackCollision.get(i).charAt(1)-'0';
                            if(mainBoard[xtran][ytran].getPiece() instanceof King && mainBoard[xtran][ytran].getPiece().getTeam()==false)
                                blackStillHit=true;
                        }
                    }
                }
            }
            //Checking if BLACK KING is still hit:
            if(blackStillHit){
                blackCheckMate=true;
            }

            //If the BLACK KING is not hit, however the number of collisions was 2 or higher, we still have a checkmate:
            if(blackCheckMate == false && blackCollisionNum > 1){
                //CHECK AGAIN IF BLACK KING CAN MOVE
                blackCheckMate = true;
            }

        }

        //Processing return statements:

        if(blackCheckMate == true && whiteCheckMate == true){
            return "bothCheckMate"; //STALEMATE
        }else if(blackCheckMate == true && whiteCheckMate == false){
            return "blackCheckMate"; //Black team loses
        }else if(blackCheckMate == false && whiteCheckMate == true){
            return "whiteCheckMate"; //White team loses
        }else if(blackCheckMate == false && whiteCheckMate == false){
            if(blackChecked == true && whiteChecked == true){
                return "bothChecked"; //Both kings are under check
            }else if(blackChecked == true && whiteChecked == false){
                return "blackChecked";
            }else if(blackChecked == false && whiteChecked == true){
                return "whiteChecked";
            }else if(blackChecked == false && whiteChecked == false){
                return "noCheck"; //Everything is fine.
            }
        }

        return "noCheck";
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}

