package com.example.tgood.androidchess76;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import control.GamePlay;
import control.GameSave;
import model.BoardAdapter;
import model.King;
import model.NonPiece;
import model.Square;

/**
 * This activity is largely the same as regular chess game,
 * however the sequence of turns is saved in a String ArrayList.
 * At the concluson of the game, this ArrayList is serialized.
 *
 */

public class RecordActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private GamePlay game;
    private GridView board;
    private PopupWindow endpop;
    private LayoutInflater layoutInflater;
    private RelativeLayout relativeLayout;
    private TextView gameMessage;
    private BoardAdapter adapter;
    private static boolean newgame = true;
    private View[] selectedViews;
    private Square[] selectedSquares;
    private String[] moveCoordinates;
    private Square[][] undoBoard = new Square[8][8];
    private boolean drawRequest=false;
    private ArrayList<String> turns;
    private int[] RankFile; //0 = initial rank, 1 = initial file, 2 = move rank, 3 = move file
    private final String[] coordinates = new String[]{
            "a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8",
            "a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7",
            "a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6",
            "a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5",
            "a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4",
            "a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3",
            "a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2",
            "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1",};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record);

        if(newgame){
            this.game = new GamePlay();
            game.setTurnCount(1);
            adapter = new BoardAdapter(this, game.getBoard());
            selectedViews = new View[2];
            moveCoordinates = new String[2];
            selectedSquares = new Square[2];
            turns = new ArrayList<String>();
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat simpleDate = new SimpleDateFormat("MM-dd-yyyy");
            String currDate = simpleDate.format(calendar.getTime());
            turns.add(currDate); //The first entry of the turns arraylist is always the date
            RankFile = new int[4];
            newgame = false;
        }

        GridView gridview = (GridView) findViewById(R.id.gridview1);
        gridview.setAdapter(adapter);
        gridview.setOnItemClickListener(this);
        this.board = gridview;
        TextView text = (TextView) findViewById(R.id.recordMessage);
        this.gameMessage = text;
        undoBoard = game.getBoard();
        initializeAI();
        initializeDraw();
        initializeResign();
        initializeUndo();
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        int column = -1;
        String moveResult;
        boolean legal = true;
        drawRequest=false;
        Square[][] prevTurn = new Square[8][8];

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                Square newSquare = new Square(i, j);
                newSquare.setPiece(game.getBoard()[i][j].getPiece());
                prevTurn[i][j] = newSquare;
            }
        }

        //Getting the Initial Position:

        if (selectedViews[0] == null) {

            column = (position/8) % 2;
            Square selectedSquare = game.getBoard()[position/8][position % 8];

            if (selectedSquare.getPiece() instanceof NonPiece) {
                return; //User clicked on an empty square
            }else{
                if(game.getTurnCount()%2 == 1){
                    if (!selectedSquare.getPiece().getTeam()) {
                        gameMessage.setText("White's move.");
                        return; //white clicked black piece
                    }
                }else if (game.getTurnCount()%2 == 0){
                    if (selectedSquare.getPiece().getTeam()) {
                        gameMessage.setText("Black's move.");
                        return; //black clicked white piece
                    }
                }
            }
            moveCoordinates[0] = coordinates[position];
            selectedViews[0] = view;
            selectedSquares[0] = selectedSquare;
            RankFile[0] = selectedSquares[0].getRank();
            RankFile[1] = selectedSquares[0].getFile();
            view.setBackgroundColor(Color.parseColor("#ff4d4d"));

        }else{//Getting the Move Position:
            undoBoard = prevTurn;
            Square selectedSquare = game.getBoard()[position/8][position%8];
            selectedViews[1] = view;
            selectedSquares[1] = selectedSquare;
            moveCoordinates[1] = coordinates[position];
            RankFile[2] = selectedSquares[1].getRank();
            RankFile[3] = selectedSquares[1].getFile();

            String coordinate = moveCoordinates[0] + " " + moveCoordinates[1];

            //Attempt to execute the move:

            Square[][] oldBoard = game.getBoard();
            Square[][] savedBoard = oldBoard;
            Square[][] newBoard = selectedSquares[0].getPiece().move(coordinate, oldBoard, game.getTurnCount(), game.getClearLayout());

            if(newBoard[RankFile[0]][RankFile[1]].getPiece() instanceof NonPiece){

                //Legal movement, updating the board:
                NonPiece newPiece = new NonPiece();
                newPiece.name = "blank";

                Square newSquare = new Square(RankFile[0], RankFile[1]);
                newSquare.setPiece(newPiece);
                newBoard[RankFile[0]][RankFile[1]] = newSquare;
                game.setBoard(newBoard);
                this.adapter.notifyDataSetChanged();
                this.board.setAdapter(adapter);
                //update turn count
                game.setTurnCount(game.getTurnCount()+1);
                turns.add(coordinate);
                gameMessage.setText("Sucessful move!");
            }else{
                //Illegal move:
                gameMessage.setText("Illegal move! Try again.");

            }
            //if movement was legal, determine game status------------------------
            moveResult=isKingChecked(game.getBoard());
            //System.out.println(moveResult);
            switch(moveResult){
                case "blackChecked"	:
                    //illegal if black is left in check as a result of own move
                    if(game.getTurnCount()%2 == 1){
                        legal=false;
                        break;
                    }
                    gameMessage.setText("Black is in Check");
                    break;

                case "whiteChecked" :
                    //illegal if white is left in check as a result of own move
                    if(game.getTurnCount()%2 == 0){
                        legal=false;
                        break;
                    }
                    gameMessage.setText("White is in Check");
                    break;

                case "bothChecked"	:
                    //illegal move
                    legal=false;
                    break;
                case "blackCheckMate" :
                    if(game.getTurnCount()%2 == 1){
                        legal=false;
                        break;
                    }
                    //black is checkmated, white wins
                    gameMessage.setText("White wins");
                    return;
                case "whiteCheckMate" :
                    if(game.getTurnCount()%2 == 0){
                        legal=false;
                        break;
                    }
                    //white is checkmated, white wins
                    gameMessage.setText("Black wins");
                    return;
                case "StaleMate"	:
                    //Stalemate...
                    System.out.println("Stalemate");
                    return;

                default:
                    //continue normally if no conditions are met
                    break;

            }
            //undo move if illegal
            if(!legal){
                this.game.setBoard(prevTurn);
                //this.adapter.notifyDataSetChanged();
                //this.board.setAdapter(adapter);
                adapter = new BoardAdapter(this, prevTurn);
                board.setAdapter(adapter);
                game.setTurnCount(game.getTurnCount()-1);
                gameMessage.setText("Illegal move, king in check.");
                if(turns.size() != 0) {
                    turns.remove(coordinate);
                }

            }

            //Gong back to initial state after processing move:

            if (column == 0) {
                if (position % 2 == 0) {
                    selectedViews[0].setBackgroundColor(Color.parseColor("#ffffff"));
                } else {
                    selectedViews[0].setBackgroundColor(Color.parseColor("#808080"));
                }
            } else {

                if (position % 2 == 0) {
                    selectedViews[0].setBackgroundColor(Color.parseColor("#808080"));
                } else {
                    selectedViews[0].setBackgroundColor(Color.parseColor("#ffffff"));
                }
            }

            legal = true;
            column = -1;
            selectedViews[0] = null;
            selectedViews[1] = null;
            moveCoordinates[0] = null;
            moveCoordinates[1] = null;

        }

    }

    //IMPORT CODE-------------------------------------------------
    public String isKingChecked(Square[][] mainBoard){

        int[][] controlBoard = new int[8][8];
        int blackRow = -1;
        int blackCol = -1;
        int whiteRow = -1;
        int whiteCol = -1;
        int whiteAttacker = 0;
        int blackAttacker = 0;
        int blackCollisionNum = 0;
        int whiteCollisionNum = 0;
        ArrayList<String> whiteCollision = new ArrayList<>();
        ArrayList<String> blackCollision = new ArrayList<>();
        boolean whiteChecked = false;
        boolean blackChecked = false;
        boolean whiteCanEscape = false;
        boolean blackCanEscape = false;
        boolean whiteCheckMate = false;
        boolean blackCheckMate = false;
        boolean blackStillHit = false;
        boolean whiteStillHit = false;
        int xtran;
        int ytran;

        //Copying mainBoard's contents to controlBoard; 0 = no piece, 1 = white, 2 = black

        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){

                if(mainBoard[i][j].getPiece() instanceof NonPiece){
                    //No piece
                    controlBoard[i][j] = 0;
                }else{
                    if(mainBoard[i][j].getPiece().getTeam() == false){
                        if(mainBoard[i][j].getPiece() instanceof King){
                            //Black King position
                            blackRow = i;
                            blackCol = j;
                            controlBoard[i][j] = 2;
                        }else{
                            //Black piece
                            controlBoard[i][j] = 2;
                        }
                    }else if(mainBoard[i][j].getPiece().getTeam() == true){
                        if(mainBoard[i][j].getPiece() instanceof King){
                            //White King position
                            whiteRow = i;
                            whiteCol = j;
                            controlBoard[i][j] = 1;
                        }else{
                            //White piece
                            controlBoard[i][j] = 1;
                        }
                    }
                }
            }
        }

        //Making Sure both Kings can be found on the board:

        if(blackRow == -1 && blackCol == -1){
            return "White wins";
        }else if(whiteRow == -1 && whiteCol == -1){
            return "Black wins";
        }

        //----------------CHECKING WHITE KING----------------

        //projecting black pieces:
        for(int p = 0; p < 8; p++){
            for (int q = 0; q < 8; q++){
                //Identifying black piece:
                if(mainBoard[p][q].getPiece().getTeam() == false){
                    //Projecting black piece:
                    whiteCollision = mainBoard[p][q].getPiece().populateTrajectory(p,  q,  mainBoard, controlBoard);
                    for(int i=0; i<whiteCollision.size(); i++){
                        xtran=whiteCollision.get(i).charAt(0)-'0';
                        ytran=whiteCollision.get(i).charAt(1)-'0';
                        if(mainBoard[xtran][ytran].getPiece() instanceof King && mainBoard[xtran][ytran].getPiece().getTeam())
                            whiteCollisionNum++;
                    }
                }
            }
        }

        //Checking if any of the black pieces can reach the WHITE KING:

        if(whiteCollisionNum > 0)
            whiteChecked=true;

        //Checking if the WHITE KING can escape:

        if(whiteChecked == true){

            if((whiteRow - 1 >= 0) && (whiteCol - 1>=0)){
                if(controlBoard[whiteRow-1][whiteCol-1] != 2
                        && mainBoard[whiteRow-1][whiteCol-1].getPiece() instanceof NonPiece){
                    whiteCanEscape = true;
                }
            }
            if(whiteRow -1 >= 0){
                if(controlBoard[whiteRow-1][whiteCol] != 2
                        && mainBoard[whiteRow-1][whiteCol].getPiece() instanceof NonPiece){
                    whiteCanEscape = true;
                }
            }
            if(whiteRow - 1 >= 0 && whiteCol + 1 < 8){
                if(controlBoard[whiteRow-1][whiteCol+1] != 2
                        && mainBoard[whiteRow-1][whiteCol+1].getPiece() instanceof NonPiece){
                    whiteCanEscape = true;
                }
            }
            if(whiteCol + 1 < 8){
                if(controlBoard[whiteRow][whiteCol+1] != 2
                        && mainBoard[whiteRow][whiteCol+1].getPiece() instanceof NonPiece){
                    whiteCanEscape = true;
                }
            }
            if(whiteRow + 1 < 8 && whiteCol + 1 < 8){
                if(controlBoard[whiteRow+1][whiteCol+1] != 2
                        && mainBoard[whiteRow+1][whiteCol+1].getPiece() instanceof NonPiece){
                    whiteCanEscape = true;
                }
            }
            if(whiteRow + 1 < 8){
                if(controlBoard[whiteRow + 1][whiteCol] != 2
                        && mainBoard[whiteRow+1][whiteCol].getPiece() instanceof NonPiece){
                    whiteCanEscape = true;
                }
            }
            if(whiteRow + 1 < 8 && whiteCol - 1 >= 0){
                if(controlBoard[whiteRow+1][whiteCol-1] != 2
                        && mainBoard[whiteRow+1][whiteCol-1].getPiece() instanceof NonPiece){
                    whiteCanEscape = true;
                }
            }
            if(whiteCol -1 >= 0){
                if(controlBoard[whiteRow][whiteCol-1] != 2
                        && mainBoard[whiteRow][whiteCol-1].getPiece() instanceof NonPiece){
                    whiteCanEscape = true;
                }
            }

        }

        //If WHITE KING is checked and can't escape, checking if other pieces can defend him:

        if(whiteChecked == true && whiteCanEscape == false){

            //Resetting the controlBoard; 0 = no piece, 1 = white, 2 = black

            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){

                    if(mainBoard[i][j].getPiece() instanceof NonPiece){
                        //No piece
                        controlBoard[i][j] = 0;
                    }else{
                        if(mainBoard[i][j].getPiece().getTeam() == false){
                            if(mainBoard[i][j].getPiece() instanceof King){
                                //Black King position
                                blackRow = i;
                                blackCol = j;
                                controlBoard[i][j] = 2;
                            }else{
                                //Black piece
                                controlBoard[i][j] = 2;
                            }
                        }else if(mainBoard[i][j].getPiece().getTeam() == true){
                            if(mainBoard[i][j].getPiece() instanceof King){
                                //White King position
                                whiteRow = i;
                                whiteCol = j;
                                controlBoard[i][j] = 1;
                            }else{
                                //White piece
                                controlBoard[i][j] = 1;
                            }
                        }
                    }
                }
            }

            //Projecting the WHITE defenders to simulate blocking:

            for(int m = 0; m < 8; m++){
                for(int n = 0; n < 8; n++){
                    //Identifying white piece:
                    if(mainBoard[m][n].getPiece().getTeam() == true){
                        //Projecting white piece:
                        mainBoard[m][n].getPiece().populateTrajectory(m, n, mainBoard, controlBoard);
                    }
                }
            }

            //Projecting the BLACK attackers to see if white king is still hit:

            for(int m = 0; m < 8; m++){
                for(int n = 0; n < 8; n++){
                    //Identifying black piece:
                    if(mainBoard[m][n].getPiece().getTeam() == false){
                        //Projecting black piece:
                        whiteCollision = mainBoard[m][n].getPiece().populateTrajectory(m, n, mainBoard, controlBoard);
                        for(int i=0; i<whiteCollision.size(); i++){
                            xtran=whiteCollision.get(i).charAt(0)-'0';
                            ytran=whiteCollision.get(i).charAt(1)-'0';
                            if(mainBoard[xtran][ytran].getPiece() instanceof King && mainBoard[xtran][ytran].getPiece().getTeam())
                                whiteStillHit=true;
                        }
                    }
                }
            }

            //Checking if the WHITE KING is still hit:

            if(whiteStillHit)
                whiteCheckMate=true;

            //If the WHITE KING is not hit, however the number of collisions was 2 or higher, we still have a checkmate:

            if(whiteCheckMate == false && whiteCollisionNum > 1){
                whiteCheckMate = true;
            }
        }

        //----------------CHECKING BLACK KING----------------

        //Resetting the controlBoard; 0 = no piece, 1 = white, 2 = black

        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){

                if(mainBoard[i][j].getPiece() instanceof NonPiece){
                    //No piece
                    controlBoard[i][j] = 0;
                }else{
                    if(mainBoard[i][j].getPiece().getTeam() == false){
                        if(mainBoard[i][j].getPiece() instanceof King){
                            //Black King position
                            blackRow = i;
                            blackCol = j;
                            controlBoard[i][j] = 2;
                        }else{
                            //Black piece
                            controlBoard[i][j] = 2;
                        }
                    }else if(mainBoard[i][j].getPiece().getTeam() == true){
                        if(mainBoard[i][j].getPiece() instanceof King){
                            //White King position
                            whiteRow = i;
                            whiteCol = j;
                            controlBoard[i][j] = 1;
                        }else{
                            //White piece
                            controlBoard[i][j] = 1;
                        }
                    }
                }
            }
        }

        //Checking whether the BLACK KING is checked:

        //projecting white pieces:
        for(int p = 0; p < 8; p++){
            for (int q = 0; q < 8; q++){
                //Identifying white piece:
                if(mainBoard[p][q].getPiece().getTeam() == true){
                    //Projecting white piece:
                    blackCollision = mainBoard[p][q].getPiece().populateTrajectory(p,  q,  mainBoard, controlBoard);
                    for(int i=0; i<blackCollision.size(); i++){
                        xtran=blackCollision.get(i).charAt(0)-'0';
                        ytran=blackCollision.get(i).charAt(1)-'0';
                        if(mainBoard[xtran][ytran].getPiece() instanceof King && mainBoard[xtran][ytran].getPiece().getTeam()==false)
                            blackCollisionNum++;
                    }
                }
            }
        }
        //TESTING
        //System.out.println("white attackers: " + blackCollisionNum +"");

        //Checking if any of the white pieces can reach the BLACK KING:
        if(blackCollisionNum>0){
            blackChecked = true;
        }
        //Checking if the BLACK KING can escape:
        if(blackChecked == true){

            if((blackRow - 1 >= 0) && (blackCol - 1>=0)){
                if(controlBoard[blackRow-1][blackCol-1] != 1
                        && (mainBoard[blackRow-1][blackCol-1].getPiece() instanceof NonPiece)){
                    blackCanEscape = true;
                }
            }
            if(blackRow -1 >= 0){
                if(controlBoard[blackRow-1][blackCol] != 1
                        && (mainBoard[blackRow-1][blackCol].getPiece() instanceof NonPiece)){
                    blackCanEscape = true;
                }
            }
            if(blackRow - 1 >= 0 && blackCol + 1 < 8){
                if(controlBoard[blackRow-1][blackCol+1] != 1
                        && (mainBoard[blackRow-1][blackCol+1].getPiece() instanceof NonPiece)){
                    blackCanEscape = true;
                }
            }
            if(blackCol + 1 < 8){
                if(controlBoard[blackRow][blackCol+1] != 1
                        && (mainBoard[blackRow][blackCol+1].getPiece() instanceof NonPiece)){
                    blackCanEscape = true;
                }
            }
            if(blackRow + 1 < 8 && blackCol + 1 < 8){
                if(controlBoard[blackRow+1][blackCol+1] != 1
                        && (mainBoard[blackRow+1][blackCol+1].getPiece() instanceof NonPiece)){
                    blackCanEscape = true;
                }
            }
            if(blackRow + 1 < 8){
                if(controlBoard[blackRow + 1][blackCol] != 1
                        && (mainBoard[blackRow+1][blackCol].getPiece() instanceof NonPiece)){
                    blackCanEscape = true;
                }
            }
            if(blackRow + 1 < 8 && blackCol - 1 >= 0){
                if(controlBoard[blackRow+1][blackCol-1] != 1
                        && (mainBoard[blackRow+1][blackCol-1].getPiece() instanceof NonPiece)){
                    blackCanEscape = true;
                }
            }
            if(blackCol - 1 >= 0){
                if(controlBoard[blackRow][blackCol-1] != 1
                        && (mainBoard[blackRow][blackCol-1].getPiece() instanceof NonPiece)){
                    blackCanEscape = true;
                }
            }

        }

        //If BLACK KING is checked and can't escape, checking if other pieces can defend him:



        if(blackChecked == true && blackCanEscape == false){

            //Resetting the controlBoard; 0 = no piece, 1 = white, 2 = black
            for(int i = 0; i < 8; i++){
                for(int j = 0; j < 8; j++){

                    if(mainBoard[i][j].getPiece() instanceof NonPiece){
                        //No piece
                        controlBoard[i][j] = 0;
                    }else{
                        if(mainBoard[i][j].getPiece().getTeam() == false){
                            if(mainBoard[i][j].getPiece() instanceof King){
                                //Black King position
                                blackRow = i;
                                blackCol = j;
                                controlBoard[i][j] = 2;
                            }else{
                                //Black piece
                                controlBoard[i][j] = 2;
                            }
                        }else if(mainBoard[i][j].getPiece().getTeam() == true){
                            if(mainBoard[i][j].getPiece() instanceof King){
                                //White King position
                                whiteRow = i;
                                whiteCol = j;
                                controlBoard[i][j] = 1;
                            }else{
                                //White piece
                                controlBoard[i][j] = 1;
                            }
                        }
                    }
                }
            }

            //Projecting the BLACK defenders:
            for(int p = 0; p < 8; p++){
                for (int q = 0; q < 8; q++){
                    //Identifying black piece:
                    if(mainBoard[p][q].getPiece().getTeam() == false){
                        //Projecting black piece:
                        mainBoard[p][q].getPiece().populateTrajectory(p,  q,  mainBoard, controlBoard);
                    }
                }
            }

            //Projecting WHITE attackers:
            for(int m = 0; m < 8; m++){
                for(int n = 0; n < 8; n++){
                    //Identifying white piece:
                    if(mainBoard[m][n].getPiece().getTeam() == true){
                        //Projecting white piece:
                        blackCollision = mainBoard[m][n].getPiece().populateTrajectory(m, n, mainBoard, controlBoard);
                        for(int i=0; i<blackCollision.size(); i++){
                            xtran=blackCollision.get(i).charAt(0)-'0';
                            ytran=blackCollision.get(i).charAt(1)-'0';
                            if(mainBoard[xtran][ytran].getPiece() instanceof King && mainBoard[xtran][ytran].getPiece().getTeam()==false)
                                blackStillHit=true;
                        }
                    }
                }
            }
            //Checking if BLACK KING is still hit:
            if(blackStillHit){
                blackCheckMate=true;
            }

            //If the BLACK KING is not hit, however the number of collisions was 2 or higher, we still have a checkmate:
            if(blackCheckMate == false && blackCollisionNum > 1){
                //CHECK AGAIN IF BLACK KING CAN MOVE
                blackCheckMate = true;
            }

        }

        //Processing return statements:

        if(blackCheckMate == true && whiteCheckMate == true){
            return "bothCheckMate"; //STALEMATE
        }else if(blackCheckMate == true && whiteCheckMate == false){
            return "blackCheckMate"; //Black team loses
        }else if(blackCheckMate == false && whiteCheckMate == true){
            return "whiteCheckMate"; //White team loses
        }else if(blackCheckMate == false && whiteCheckMate == false){
            if(blackChecked == true && whiteChecked == true){
                return "bothChecked"; //Both kings are under check
            }else if(blackChecked == true && whiteChecked == false){
                return "blackChecked";
            }else if(blackChecked == false && whiteChecked == true){
                return "whiteChecked";
            }else if(blackChecked == false && whiteChecked == false){
                return "noCheck"; //Everything is fine.
            }
        }

        return "noCheck";
    }

    private void initializeAI(){

        Button AIButton = (Button) findViewById(R.id.AI1);
        AIButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View argo){
                automove();
            }

        });
    }

    private void initializeResign(){

        Button resignButton = (Button) findViewById(R.id.resign1);
        resignButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View argo){
                drawRequest=false;
                resign();
            }
        });

    }

    private void initializeUndo(){

        Button undoButton = (Button) findViewById(R.id.undo1);
        undoButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View argo){
                drawRequest=false;
                undo();
            }
        });

    }

    private void initializeDraw(){

        Button drawButton = (Button) findViewById(R.id.draw1);
        drawButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View argo){
                //Draw
                if(drawRequest){//draw accepted
                    if(game.getTurnCount()%2 == 1){
                        gameMessage.setText("White accepts the draw. ");
                    }else if (game.getTurnCount()%2 == 0){
                        gameMessage.setText("Black accepts the draw.");
                    }
                    saveGame();
                    return;
                }
                drawRequest=true;//draw requested
                if(game.getTurnCount()%2 == 1){
                    gameMessage.setText("White requests a draw. Accept?");
                }else if (game.getTurnCount()%2 == 0){
                    gameMessage.setText("Black requests a draw. Accept?");
                }
            }
        });

    }

    private void automove(){

    }

    private void resign(){
        if(game.getTurnCount()%2 == 1){
            gameMessage.setText("White resigns.");
        }else if (game.getTurnCount()%2 == 0){
            gameMessage.setText("Black resigns.");
        }
        saveGame();
    }

    private void undo(){
        if(undoBoard==game.getBoard()) {
            gameMessage.setText("can't undo");
            return;
        }
        this.game.setBoard(undoBoard);
        adapter = new BoardAdapter(this, undoBoard);
        board.setAdapter(adapter);
        game.setTurnCount(game.getTurnCount()-1);

        //Removing the move from the ArrayList:

        if(turns.size() > 2) {
            turns.remove(turns.size() - 1);
        }

        gameMessage.setText("move undone");
    }


    /**
     * At the end of the game, the game will be saved using a GameSave object.
     */

    public void saveGame(){

        if(turns != null) {
            GameSave test1 = new GameSave(turns);

        }else{

        }
    }

}





