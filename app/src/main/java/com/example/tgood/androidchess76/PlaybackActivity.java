package com.example.tgood.androidchess76;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import control.GameSave;
import model.ListviewAdapter;

public class PlaybackActivity extends AppCompatActivity {

    private List<List<String>> gameCollection;

    public void onCreate(Bundle saveInstanceState)
    {
        super.onCreate(saveInstanceState);
        setContentView(R.layout.activity_playback);

        // Get the reference of ListViewAnimals
        ListView list =(ListView)findViewById(R.id.listview);
        GameSave saved = new GameSave();
        gameCollection = saved.loadGame();
        if(gameCollection != null && gameCollection.size() > 0){

            //Extracting the dates from gameCollections and putting them into an ArrayList:

            ArrayList<String> dates = new ArrayList<String>();

            for(int i = 0; i < gameCollection.size(); i++){
                dates.add(gameCollection.get(i).get(0));
            }

            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,R.layout.simple_text, dates);
            list.setAdapter(arrayAdapter);

            list.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
                    ArrayList<String> selectedGame = (ArrayList<String>) gameCollection.get(position);
                    Toast.makeText(getApplicationContext(), "Game selected : "+ selectedGame.get(0), Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(PlaybackActivity.this, SavedGameActivity.class);
                    intent.putExtra("FILES_TO_SEND", selectedGame);
                    startActivity(intent);
                }
            });

        }else{

            ArrayList<String> nodata = new ArrayList<String>();
            nodata.add("No gamesaves were found.");
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,R.layout.simple_text, nodata);
            list.setAdapter(arrayAdapter);

        }

    }
}

