package control;

import android.widget.ImageView;

import model.Bishop;
import model.King;
import model.Knight;
import model.NonPiece;
import model.Pawn;
import model.Piece;
import model.Queen;
import model.Rook;
import model.Square;

public class GamePlay {

    private Square[][] boardLayout;
    private Square[][] clearLayout;
    private int turnCount;

    //Constructor
    public GamePlay(){

        boardLayout = new Square[8][8];
        clearLayout = new Square[8][8];

        //initializing boardLayout with empty Squares:

        for(int i = 0; i < 8; i++){
            for (int j = 0; j < 8; j++){
                boardLayout[i][j] = new Square(i, j);
                boardLayout[i][j].setRank(i);
                boardLayout[i][j].setFile(j);
            }
        }

        //Initializing clearLayout:

        for(int m = 0; m < 8; m++){
            for(int n = 0; n < 8; n++){
                NonPiece temp = new NonPiece();
                temp.name = "blank";
                Square temp1 = new Square(m, n);
                temp1.setPiece(temp);
                clearLayout[m][n] = temp1;
                clearLayout[m][n].setRank(m);
                clearLayout[m][n].setFile(n);

            }
        }

        //Setting up fresh board:

        initializeBoard();
    }

    private void initializeBoard(){

        Rook bRl = new Rook();
            bRl.name = "blackrook";
            bRl.white = false;
        Rook bRr = new Rook();
            bRr.name = "blackrook";
            bRr.white = false;
        Rook wRl = new Rook();
            wRl.white = true;
            wRl.name = "whiterook";
        Rook wRr = new Rook();
            wRr.white = true;
            wRr.name = "whiterook";

        boardLayout[0][0].setPiece(bRl);
        boardLayout[0][7].setPiece(bRr);
        boardLayout[7][0].setPiece(wRl);
        boardLayout[7][7].setPiece(wRr);

        Knight bNl = new Knight();
            bNl.name = "blackknight";
        Knight bNr = new Knight();
            bNr.name = "blackknight";
        Knight wNl = new Knight();
            wNl.white = true;
            wNl.name = "whiteknight";
        Knight wNr = new Knight();
            wNr.white = true;
            wNr.name = "whiteknight";

        boardLayout[0][1].setPiece(bNl);
        boardLayout[0][6].setPiece(bNr);
        boardLayout[7][1].setPiece(wNl);
        boardLayout[7][6].setPiece(wNr);

        Bishop bBl = new Bishop();
            bBl.name = "blackbishop";
        Bishop bBr = new Bishop();
            bBr.name = "blackbishop";
        Bishop wBl = new Bishop();
            wBl.white = true;
            wBl.name = "whitebishop";
        Bishop wBr = new Bishop();
            wBr.white = true;
            wBr.name = "whitebishop";

        boardLayout[0][2].setPiece(bBl);
        boardLayout[0][5].setPiece(bBr);
        boardLayout[7][2].setPiece(wBl);
        boardLayout[7][5].setPiece(wBr);

        Queen bQ = new Queen();
            bQ.name = "blackqueen";
        Queen wQ = new Queen();
            wQ.white = true;
            wQ.name = "whitequeen";

        boardLayout[0][3].setPiece(bQ);
        boardLayout[7][3].setPiece(wQ);

        King bK = new King();
            bK.name = "blackking";
        King wK = new King();
            wK.white = true;
            wK.name = "whiteking";

        boardLayout[0][4].setPiece(bK);
        boardLayout[7][4].setPiece(wK);

        Pawn bp0 = new Pawn();
            bp0.name = "blackpawn";
        Pawn bp1 = new Pawn();
            bp1.name = "blackpawn";
        Pawn bp2 = new Pawn();
            bp2.name = "blackpawn";
        Pawn bp3 = new Pawn();
            bp3.name = "blackpawn";
        Pawn bp4 = new Pawn();
            bp4.name = "blackpawn";
        Pawn bp5 = new Pawn();
            bp5.name = "blackpawn";
        Pawn bp6 = new Pawn();
            bp6.name = "blackpawn";
        Pawn bp7 = new Pawn();
            bp7.name = "blackpawn";
        Pawn wp0 = new Pawn();
            wp0.white = true;
            wp0.name = "whitepawn";
        Pawn wp1 = new Pawn();
            wp1.white = true;
            wp1.name = "whitepawn";
        Pawn wp2 = new Pawn();
            wp2.white = true;
            wp2.name = "whitepawn";
        Pawn wp3 = new Pawn();
            wp3.white = true;
            wp3.name = "whitepawn";
        Pawn wp4 = new Pawn();
            wp4.white = true;
            wp4.name = "whitepawn";
        Pawn wp5 = new Pawn();
            wp5.white = true;
            wp5.name = "whitepawn";
        Pawn wp6 = new Pawn();
            wp6.white = true;
            wp6.name = "whitepawn";
        Pawn wp7 = new Pawn();
            wp7.white = true;
            wp7.name = "whitepawn";

        boardLayout[1][0].setPiece(bp0);
        boardLayout[1][1].setPiece(bp1);
        boardLayout[1][2].setPiece(bp2);
        boardLayout[1][3].setPiece(bp3);
        boardLayout[1][4].setPiece(bp4);
        boardLayout[1][5].setPiece(bp5);
        boardLayout[1][6].setPiece(bp6);
        boardLayout[1][7].setPiece(bp7);

        boardLayout[6][0].setPiece(wp0);
        boardLayout[6][1].setPiece(wp1);
        boardLayout[6][2].setPiece(wp2);
        boardLayout[6][3].setPiece(wp3);
        boardLayout[6][4].setPiece(wp4);
        boardLayout[6][5].setPiece(wp5);
        boardLayout[6][6].setPiece(wp6);
        boardLayout[6][7].setPiece(wp7);

        //Populating the empty section:

        for(int j = 1; j < 8; j+=2){

            NonPiece temp = new NonPiece();
            NonPiece temp1 = new NonPiece();

            temp.name = "blank";
            temp1.name = "blank";

            boardLayout[2][j].setPiece(temp);
            boardLayout[2][j-1].setPiece(temp1);
            boardLayout[4][j].setPiece(temp);
            boardLayout[4][j-1].setPiece(temp1);
            boardLayout[3][j-1].setPiece(temp);
            boardLayout[3][j].setPiece(temp1);
            boardLayout[5][j-1].setPiece(temp);
            boardLayout[5][j].setPiece(temp1);

        }
    }

    public Square[][] getBoard(){
        return boardLayout;
    }

    public void setBoard(Square[][] board){
        this.boardLayout = board;
    }

    public Square[][] getClearLayout(){
        return clearLayout;
    }

    public int getTurnCount(){
        return turnCount;
    }

    public void setTurnCount(int turnCount){
        this.turnCount = turnCount;
    }


}
