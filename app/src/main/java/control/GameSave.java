package control;

import android.content.Context;

import com.example.tgood.androidchess76.MyApplication;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class GameSave implements Serializable {

    private static final long serialVersionUID = 1L;
    private ArrayList<String> turns;
    private List<List<String>> gameCollection;
    private HashMap<String, ArrayList<String>> savedGame;
    private File file;
    private String fileName = "GameSaves.ser";

    //Constructor
    public GameSave(ArrayList<String> turns){
        this.turns = turns;
        this.gameCollection = loadGame();
        if(gameCollection != null) {
            gameCollection.add(turns);
        }else{
            gameCollection = new ArrayList<List<String>>();
            gameCollection.add(turns);
        }
        saveGame();
    }

    public GameSave(){}

    //Saving

    public void saveGame(){

        try {
            Context context = MyApplication.getAppContext();
            FileOutputStream outputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE) ;
            ObjectOutputStream oStream = new ObjectOutputStream(outputStream);
            oStream.writeObject(gameCollection);
            oStream.flush();
            oStream.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    //Loading

    public List<List<String>> loadGame(){
        List<List<String>> game = null;

        try{
            Context context = MyApplication.getAppContext();
            FileInputStream fileInputStream = context.openFileInput(fileName);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            game = (List<List<String>>) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();

        }catch(Exception e){
            e.printStackTrace();
        }

        return game;
    }
}
